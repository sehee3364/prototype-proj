/* SystemJS module definition */
declare var module: NodeModule;

declare var vis: any;
declare var moment: any;
declare var $ : any;
declare var jui : any;
declare var L : any;
declare var mapboxgl : any;
declare var d3: any;
declare var google: any;
declare var MarkerClusterer: any;
//declare const svgPanZoom: any;

interface NodeModule {
  id: string;
}
