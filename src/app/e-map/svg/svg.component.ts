import { Component, OnInit } from '@angular/core';
//declare const svgPanZoom: any;

@Component({
  selector: 'app-svg',
  templateUrl: './svg.component.html',
  styleUrls: ['./svg.component.css']
})
export class SvgComponent implements OnInit {
  panZoomInstance;

  xscale;
  yscale;
  map;
  imagelayer;
  heatmap;
  vectorfield;
  pathplot;
  overlays;
  mapdata = {};

  jsonData = {
    "heatmap": { // 색으로 구분된 셀
      "binSize": 3,
      "units": "\u00B0C",
      "map": [{"x": 21, "y": 12, "value": 10.2},
        {"x": 24, "y": 12, "value": 19.9},
        {"x": 27, "y": 12, "value": 19.7},
        {"x": 30, "y": 12, "value": 19.7},
        {"x": 21, "y": 15, "value": 20.5},
        {"x": 24, "y": 15, "value": 19.3},
        {"x": 27, "y": 15, "value": 19.4},
        {"x": 30, "y": 15, "value": 19.9},
        {"value": 19.9, "points": [{"x":2.513888888888882,"y":8.0},
            {"x":6.069444444444433,"y":8.0},
            {"x":6.069444444444434,"y":5.277535934291582},
            {"x":8.20833333333332,"y":2.208151950718685},
            {"x":13.958333333333323,"y":2.208151950718685},
            {"x":16.277777777777825,"y":5.277535934291582},
            {"x":16.277777777777803,"y":10.08151950718685},
            {"x":17.20833333333337,"y":10.012135523613962},
            {"x":17.27777777777782,"y":18.1387679671458},
            {"x":2.513888888888882,"y":18.0}]}]
    },
    "vectorfield": { // 크기 조절된 벡터 배열
      "binSize": 3,
      "units": "ft/s",
      "map": [{"x": 18, "y": 21, "value": {"x": 4, "y": 3}},
        {"x": 21, "y": 21, "value": {"x": 3, "y": 3}},
        {"x": 18, "y": 24, "value": {"x": 1, "y": 2}},
        {"x": 21, "y": 24, "value": {"x": -3, "y": 4}},
        {"x": 24, "y": 24, "value": {"x": -4, "y": 1}}]
    },
    "pathplot": [{ // 점 사이 직선 연결
      "id": "flt-1",
      "classes": "planned",
      "points": [{"x": 23.8, "y": 30.6},
        {"x": 19.5, "y": 25.7},
        {"x": 14.5, "y": 25.7},
        {"x": 13.2, "y": 12.3}]
    }],
    "overlays": {
      "polygons": [
        {"id": "p1", "name": "kitchen",
          "points": [{"x":2.513888888888882,"y":8.0},
            {"x":6.069444444444433,"y":8.0},
            {"x":6.069444444444434,"y":5.277535934291582},
            {"x":8.20833333333332,"y":2.208151950718685},
            {"x":13.958333333333323,"y":2.208151950718685},
            {"x":16.277777777777825,"y":5.277535934291582},
            {"x":16.277777777777803,"y":10.08151950718685},
            {"x":17.20833333333337,"y":10.012135523613962},
            {"x":17.27777777777782,"y":18.1387679671458},
            {"x":2.513888888888882,"y":18.0}]
        }]
    }
  }

  constructor() {
    this.xscale = d3.scale.linear().domain([0,50.0]).range([0,720]);
    this.yscale = d3.scale.linear().domain([0,33.79]).range([0,487]);
    this.map = d3.floorplan().xScale(this.xscale).yScale(this.yscale);

    this.imagelayer = d3.floorplan.imagelayer();
    this.heatmap = d3.floorplan.heatmap();
    this.vectorfield = d3.floorplan.vectorfield();
    this.pathplot = d3.floorplan.pathplot();
    this.overlays = d3.floorplan.overlays().editMode(true);
  }

  ngOnInit() {
    this.loadData(this.jsonData);
  }

  // d3-floorplan 라이브러리 이용
  loadData(data:any){
    this.map.addLayer(this.imagelayer)
      .addLayer(this.heatmap)
      .addLayer(this.vectorfield)
      .addLayer(this.pathplot)
      .addLayer(this.overlays);

    this.mapdata[this.imagelayer.id()] = [{
      url: 'https://dciarletta.github.io/d3-floorplan/Sample_Floorplan.jpg',
      x: 0,
      y: 0,
      height: 33.79,
      width: 50.0
    }];
    this.mapdata[this.heatmap.id()] = data.heatmap;
    this.mapdata[this.vectorfield.id()] = data.vectorfield;
    this.mapdata[this.pathplot.id()] = data.pathplot;
    this.mapdata[this.overlays.id()] = data.overlays;

    d3.select("#demo").append("svg")
      .attr("height", 487).attr("width",720)
      .datum(this.mapdata)                    //데이터 바인딩
      .call(this.map);                        //렌더링 / 업데이트



    var div = d3.select("body").append("div")
      .attr("class", "tooltip")
      .style("opacity", 0);

    // debugger;
    d3.select('.polygon')
      .on("mouseover", (d) => {
        div.transition()
          .duration(0)
          .style("opacity", .9);
        div .html("Price: <br>" +  "<br/>Volume: ")
          .style("left", (d3.event.pageX) + "px")
          .style("top", (d3.event.pageY - 28) + "px");})
      .on("mouseout", (d) => {
        div.transition()
          .duration(0)
          .style("opacity", 0);
      });
  }

  // svg-pan-zoom 라이브러리 이용
  initSvg() {
    $('#sample').on('load',()=>{
      this.panZoomInstance = svgPanZoom('#sample',{
        zoomEnabled: true,
        controlIconsEnabled: true,
        dblClickZoomEnabled: false,
        onZoom: (ev) => {
        },
        customEventsHandler: {
          haltEventListeners: ['click','dblclick'],
          init: (options)=>{
            var svg = options.svgElement;

            svg.addEventListener('click', event => {
              //console.log('customEventsHandler:',event.clientX, event.clientY);
              //console.log(event.target);
            });

            svg.addEventListener('dblclick', event => {
              let bound = svg.getBoundingClientRect();
              let x = event.clientX+200;
              let y = event.clientY+50;

              $('#marker:hidden').show();
              $('#marker').offset({ top:y, left:x});
            });

            // //----------------------------------------------------------------
            // svg.addEventListener('click', event => {});
            // svg.addEventListener('wheel', event => {});
            // //----------------------------------------------------------------
            // svg.addEventListener('click', event => {
            //   let bound = svg.getBoundingClientRect();
            //   let x = event.clientX - bound.left - svg.clientLeft;
            //   let y = event.clientY - bound.top - svg.clientTop;
            // });
            // //----------------------------------------------------------------
            // var pt = svg.createSVGPoint();
            // function cursorPoint(event){
            //   pt.x = event.clientX;
            //   pt.y = event.clientY;
            //   return pt.matrixTransform(svg.getScreenCTM().inverse());
            // }
            // svg.addEventListener('click',event => {
            //   var loc = cursorPoint(event);
            //   console.log("cursorPoint",loc.x, loc.y);
            // },false);

          }
          , destroy: ()=>{
          }
        }
      });

      // this.panZoomInstance.setOnPan((level) => {
      //   console.log(level);
      // });

    });
  }
}
