import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-leaflet',
  templateUrl: './leaflet.component.html',
  styleUrls: ['./leaflet.component.css']
})
export class LeafletComponent implements OnInit {
  map;
  type:string;
  latlng:any;
  markerList = [];

  constructor() {}

  ngOnInit() {
    this.jstree();
    this.initMap();
  }

  jstree() {
    $('#jstree')
      .jstree({
      "core" : {
        "check_callback": (operation: any, node: any, parent: any, position: any, more: any) => {
          /** operation : 'create_node', 'rename_node', 'delete_node', 'move_node', 'copy_node', 'edit'*/
          return operation === 'move_node' ? false : true;
        },
        'data' : this.setTreeData(),
        'multiple' : false
      },
      "types": {
        "#": {
          "valid_children": ["folder", "device"]
        },
        "folder": {
          "valid_children": ["folder", "device"]
        },
        "device": {
          "icon": "glyphicon glyphicon-facetime-video",
          "valid_children": []
        }
      },
      "dnd":{
        is_draggable : (node, e) => {
          if( node[0].type === 'device' && this.markerList.indexOf(node[0].text) < 0 ) return true;
          else false;
        }
      },
      "plugins" : [ "changed", "types", "dnd" ]
    });
  }

  setTreeData(){
    let arr = [
      {"id": "camera1", "parent": "#", "text": "device_1", "type": "device"},
      {"id": "node1", "parent": "#", "text": "Folder_1", "type": "folder","state":{'opened' : true, 'disabled' : true}},
      {"id": "camera2", "parent": "node1", "text": "device_2", "type": "device"},
      {"id": "camera3", "parent": "node1", "text": "device_3", "type": "device"},
      {"id": "camera4", "parent": "node1", "text": "device_4", "type": "device"},
      {"id": "node2", "parent": "#", "text": "Folder_2", "type": "folder","state":{'opened' : true, 'disabled' : true}},
      {"id": "camera5", "parent": "node2", "text": "device_5", "type": "device"},
      {"id": "camera6", "parent": "node2", "text": "device_6", "type": "device"},
      {"id": "camera7", "parent": "node2", "text": "device_7", "type": "device"},
      {"id": "camera8", "parent": "node2", "text": "device_8", "type": "device"}];
    return arr;
  }

  initMap(){
    this.map = L.map('image-map', {
      minZoom: 1,
      maxZoom: 4,
      center: [0, 0],
      zoom: 1,
      dragging: true,
      doubleClickZoom: false,
      attributionControl: false
    });

    L.control.attribution({
      prefix: false
    }).addTo(this.map);

    const url = 'https://dl.dropbox.com/s/yhrpnftsuis15z6/Topkapi_Palace_plan.svg';
    const w = 1280 * 2;
    const h = 806 * 2;
    const southWest = this.map.unproject([0, h], this.map.getMaxZoom() - 1);
    const northEast = this.map.unproject([w, 0], this.map.getMaxZoom() - 1);
    const bounds = new L.LatLngBounds(southWest, northEast);

    L.imageOverlay(url, bounds).addTo(this.map);

    this.map.setMaxBounds(bounds);

    this.setMapEvt();
  }

  setMapEvt(){
    // marker :: drag
    $(document).on('dnd_start.vakata',(e,data)=>{
      this.setType('add');
    });

    // marker :: tree -> map 이동
    this.map.on('mouseover', (mapEvt)=> {
      $(document).on('dnd_move.vakata', (e, data) => {
        let t = $(data.event.target);

        if (!t.closest('.jstree').length) {
          if (t.closest('.myMap').length) {
            data.helper.find('.jstree-icon').removeClass('jstree-er').addClass('jstree-ok');
          }
          else {
            data.helper.find('.jstree-icon').removeClass('jstree-ok').addClass('jstree-er');
          }
        }
      });
    });

    // marker :: drop
    this.map.on('mouseup', (mapEvt)=> {
      this.latlng = mapEvt.latlng

      $(document).on('dnd_stop.vakata',(e,data)=>{
        let t = $(data.event.target);

        if(this.type == 'add') {
          this.addMarker(data.data.origin.get_node(data.element).text);
          this.setType('default');
        }
        $(document).off('dnd_stop.vakata');
      });
    });
  }

  addMarker(devId:string){
    const customPin = L.divIcon({
      className: 'custom-pin-group',
      html: '<div id="'+ devId +'" class="custom-pin-div">\n' +
      '\t<img src="../../assets/img/cctv_45_sky.png" class="custom-pin camera">\n' +
      '\t<img src="../../assets/img/area.png" class="custom-pin rotate" style="display: none">' +
      '\t<img src="../../assets/img/btn_close.png" class="custom-pin close" style="display: none">\n' +
      '</div>',
      iconAnchor: [30, 30]
    });

    let newMarker = L.marker(this.latlng,
      { icon: customPin,
        draggable: false,
        title: `${devId}`
      }).addTo(this.map);

    this.markerList.push(devId);
    console.log("add:", this.markerList);

    this.setMarkerEvt(newMarker);
  }

  setMarkerEvt(newMarker: any){
    newMarker.on('mousedown', ()=>{
      let markerId = newMarker.options.title;

      // marker 이동 설정
      if(this.type == 'move') newMarker.dragging.enable();
      else newMarker.dragging.disable();

      // marker 회전 설정
      if(this.type == 'rotate') {
        $('#'+markerId + ' > .rotate').css("display","inline");
        newMarker.on('mousemove',(e)=>{ this.rotateOnMouse(e, '#'+markerId); });
        newMarker.on('mouseup',  ( )=>{ newMarker.off('mousemove'); });
      }

      // marker 삭제 설정
      if(this.type == 'delete') {
        this.markerList.splice(this.markerList.indexOf(markerId),1);
        console.log("delete:", this.markerList);
        newMarker.remove();
      }
    });
  }

  setType(_type: any){
    this.type = _type;

    if(this.type == 'default') this.map.dragging.enable();
    else this.map.dragging.disable();

    if(this.type != 'rotate') $('.rotate').css("display","none");

    if(this.type == 'delete') $('.close').css("display","inline");
    else $('.close').css("display","none");
  }

  rotateOnMouse(e, marker) {
    const offset = $(marker).offset();
    const center_x = (offset.left) + ($(marker).width() / 2);
    const center_y = (offset.top) + ($(marker).height() / 2);
    const mouse_x = e.originalEvent.pageX;
    const mouse_y = e.originalEvent.pageY;
    const radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
    const degree = (radians * (180 / Math.PI) * -1) + 100;

    $(marker + ' > .custom-pin').css('-webkit-transform', 'rotate( ' + degree + 'deg)');
    $(marker + ' > .custom-pin').css('-ms-transform', 'rotate( ' + degree + 'deg)');
  }

}
