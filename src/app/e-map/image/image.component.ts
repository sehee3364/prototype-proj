import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-image',
  templateUrl: './image.component.html',
  styleUrls: ['./image.component.css']
})
export class ImageComponent implements OnInit {

  cameraIcon: string = "";
  cameraCnt: number = 0;

  ngOnInit(): void {

  }

  // 카메라 추가
  addCamera(event: any) {
    const offsetY: number = Number(event['offsetY']) - 10;
    const offsetX: number = Number(event['offsetX']) - 10;
    var cameraID: string = "camera" + (++this.cameraCnt);

    console.log(cameraID);
    // (방법 1) : 파이프 사용하여 html 속성 유지시키며, cameraList 내부에 추가
    // 하지만, 클릭할때마다 cameraIcon 새로 그려서, 그린 다음에 주는 속성 초기화됨
    //this.cameraIcon += `<i id=\"${cameraID}\" class=\"fa fa-video-camera fa-2x camera-icon\" style=\"position: absolute; top: ${offsetY}px; left: ${offsetX}px;\" aria-hidden=\"true\"></i>`;

    // (방법 2)
    $( "#cameraList" ).append( `<i id=\"${cameraID}\" class=\"fa fa-video-camera fa-2x camera-icon\" style=\"position: absolute; top: ${offsetY}px; left: ${offsetX}px;\" aria-hidden=\"true\"></i>` );
  }

  // 카메라 회전
  // 클릭할 때마다 45deg씩 회전
  rotate(cameraID: any){
    var camera = $("#"+cameraID);

    if(camera.hasClass("45")){
      camera.css("transform","rotate(90deg)"); camera.removeClass("45").addClass("90");
    }else if(camera.hasClass("90")){
      camera.css("transform","rotate(135deg)"); camera.removeClass("90").addClass("135");
    }else if(camera.hasClass("135")){
      camera.css("transform","rotate(180deg)"); camera.removeClass("135").addClass("180");
    }else if(camera.hasClass("180")){
      camera.css("transform","rotate(-135deg)"); camera.removeClass("180").addClass("-135");
    }else if(camera.hasClass("-135")){
      camera.css("transform","rotate(-90deg)"); camera.removeClass("-135").addClass("-90");
    }else if(camera.hasClass("-90")){
      camera.css("transform","rotate(-45deg)"); camera.removeClass("-90").addClass("-45");
    }else if(camera.hasClass("-45")){
      camera.css("transform","rotate(0deg)"); camera.removeClass("-45");
    }else{
      camera.css("transform","rotate(45deg)"); camera.addClass("45");
    }
  }
}
