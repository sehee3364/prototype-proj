import { NgModule } from '@angular/core';
import { Routes, RouterModule} from '@angular/router';
import { JstreeComponent } from './jstree/jstree.component';
import { AppComponent } from './app.component';
import { JqwidgetChartsComponent } from './jqwidgets/jqwidget-charts/jqwidget-charts.component';
import { JqwidgetGridComponent} from './jqwidgets/jqwidget-grid/jqwidget-grid.component';
import { JqwidgetTreeComponent} from './jqwidgets/jqwidget-tree/jqwidget-tree.component';
import { JuiChartsComponent} from './jui/jui-charts/jui-charts.component';
import { JuiGridComponent} from './jui/jui-grid/jui-grid.component';
import { RealtimeComponent } from './timeline/realtime/realtime.component';
import { PlaybackComponent } from './timeline/playback/playback.component';
import { JqwidgetWithComponent } from './jqwidgets/jqwidget-with/jqwidget-with.component';
import { GisComponent } from './gis/gis.component';
import { SvgComponent } from './e-map/svg/svg.component';
import { ImageComponent } from './e-map/image/image.component';
import { LeafletComponent } from './e-map/leaflet/leaflet.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'jqwidgets/charts',
    pathMatch: 'full'
  },
  {
    path: 'jqwidgets',
    children: [
      { path: 'charts', component: JqwidgetChartsComponent },
      { path: 'grid', component: JqwidgetGridComponent },
      { path: 'with', component: JqwidgetWithComponent },
      { path: 'tree', component: JqwidgetTreeComponent}
    ]
  },
  {
    path: 'jui',
    children : [
      { path: 'charts', component: JuiChartsComponent },
      { path: 'grid', component: JuiGridComponent }
    ]
  },
  {
    path: 'timeline',
    children : [
      { path: 'realtime', component: RealtimeComponent },
      { path: 'playback', component: PlaybackComponent }
    ]
  },
  {
    path: 'jstree',
    component: JstreeComponent
  },
  {
    path: 'gis',
    component: GisComponent
  },
  {
    path: 'e-map',
    children : [
      { path: 'svg-pan-zoom', component: SvgComponent },
      { path: 'image', component: ImageComponent },
      { path: 'leaflet', component: LeafletComponent }
    ]
  }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
