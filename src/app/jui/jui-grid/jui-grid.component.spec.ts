import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuiGridComponent } from './jui-grid.component';

describe('JuiGridComponent', () => {
  let component: JuiGridComponent;
  let fixture: ComponentFixture<JuiGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuiGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuiGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
