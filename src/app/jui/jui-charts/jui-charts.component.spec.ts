import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JuiChartsComponent } from './jui-charts.component';

describe('JuiChartsComponent', () => {
  let component: JuiChartsComponent;
  let fixture: ComponentFixture<JuiChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JuiChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JuiChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
