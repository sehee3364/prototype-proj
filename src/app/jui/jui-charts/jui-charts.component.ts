import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jui-charts',
  templateUrl: './jui-charts.component.html',
  styleUrls: ['./jui-charts.component.css']
})
export class JuiChartsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.juichart();
    this.piechart();
  }

  juichart() {
    const chart = jui.include('chart.builder');

    chart('#barchart', {
       width: 500,
       height : 500,
       theme : 'jennifer',
        axis : {
            x : {
                type : 'block',
                domain : 'quarter',
                line : true
            },
            y : {
                type : 'range',
                domain : function(d) { return [d.sales, d.profit ]; },
                step : 10,
                line : true,
                orient : 'left'
            },
            data : [
                { quarter : '1Q', sales : 50, profit : 35 },
                { quarter : '2Q', sales : 20, profit : 30 },
                { quarter : '3Q', sales : 10, profit : 5 },
                { quarter : '4Q', sales : 30, profit : 25 }
            ]
            },
            brush : {
                type : 'column',
                target : [ 'sales', 'profit' ]
            },
            widget : [
                { type : 'title', text : 'Column Sample' },
                { type : 'tooltip' },
                { type : 'legend' }
            ]
    });

  }

  piechart() {
    const chart = jui.include('chart.builder');
    const names = {
      ie: 'IE',
      ff: 'Fire Fox',
      chrome: 'Chrome',
      safari: 'Safari',
      other: 'Others'
    };
    chart('#piechart', {
      padding : 70,
      axis : {
          data : [
              { ie : 70, ff : 11, chrome : 9, safari : 6, other : 4 }
          ]
      },
      brush : {
          type : 'pie',
          showText : 'outer',
          active : 'ie',
          activeEvent : 'click',
          format : function(k, v) {
              return names[k] + ': ' + v;
          }
      },
      widget : [{
          type : 'title',
          // text : 'Pie Sample'
      }, {
          type : 'tooltip',
          orient : 'left',
          format : function(data, k) {
              return {
                  key: names[k],
                  value: data[k]
              };
          }
      }, {
          type : 'legend',
          format : function(k) {
              return names[k];
          }
      }]
    });
  }

}
