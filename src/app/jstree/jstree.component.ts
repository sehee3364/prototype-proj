import { Component, OnInit } from '@angular/core';
// import { clearTimeout, setTimeout } from 'timers';

@Component({
  selector: 'app-jstree',
  templateUrl: './jstree.component.html',
  styleUrls: ['./jstree.component.css']
})
export class JstreeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.jstree();
  }

  add () {
    const ref = $('#jstree').jstree(true);
    let sel = ref.get_selected();
      if (!sel.length) { return false; }

      sel = sel[0];
      sel = ref.create_node(sel, {'type': 'file'});

      if (sel) {
      ref.edit(sel);
      }
  }

  jstree() {

    $('#jstree').jstree({
      'checkbox' : {
        'keep_selected_style' : false
      },
      'core' : {
        'check_callback' : true
      },
      'plugins' : ['search', 'checkbox', 'dnd']
    });

    $('#search').keyup( () => {
      let timer = null;
      if (timer) { clearTimeout(timer); }
       timer = setTimeout( () => {
          const v = $('#search').val();
          $('#jstree').jstree(true).search(v);
        }, 250);
    });
  }
}
