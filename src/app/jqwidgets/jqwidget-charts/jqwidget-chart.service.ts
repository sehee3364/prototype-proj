import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { JqwidgetChartsComponent } from './jqwidget-charts.component';

@Injectable()
export class JqwidgetChartService {

  constructor(private http: Http) { }

  getList() {

      const Data = [
        {
          'chart': {
              'data': [
                  {
                      'day': 1,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 2,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 3,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 4,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 5,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 6,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  },
                  {
                      'day': 7,
                      'keith': 1391,
                      'erica': 1158,
                      'george': 1201
                  }
              ],
              'title': 'column chart'
            }
        }
    ];
  }

}
