import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JqwidgetChartService } from './jqwidget-chart.service';

@Component({
  selector: 'app-jqwidget-charts',
  templateUrl: './jqwidget-charts.component.html',
  styleUrls: ['./jqwidget-charts.component.css'],
  providers: [ JqwidgetChartService ]
})
export class JqwidgetChartsComponent implements OnInit {

  val: any;

  constructor(private jqwidgetchart: JqwidgetChartService) { }

  ngOnInit() {
    this.jqchart();
    this.jqpiechart();
    this.jqareachart();
    this.list();
  }

  list() {
    // let results: any;
    // this.jqwidgetchart.getList().subscribe(
    //   result => results = result,
    //   error => console.log(<any> error),
    //   () => {
    //     // this.Info = results['response']['body']['items']['item'];
    //     // this.jqchart(this.Info);
    //   }
    // );

  }
  jqchart() {

  this.val = $('#chart option:selected').val();
    console.log(this.val);
  let Data = {
    'data': [
        {
            'Day': 1,
            'keith': 1350,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 2,
            'keith': 1331,
            'erica': 1100,
            'george': 1200
        },
        {
            'Day': 3,
            'keith': 1391,
            'erica': 1000,
            'george': 1220
        },
        {
            'Day': 4,
            'keith': 1391,
            'erica': 1000,
            'george': 1400
        },
        {
            'Day': 5,
            'keith': 1391,
            'erica': 1700,
            'george': 2000
        },
        {
            'Day': 6,
            'keith': 1791,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 7,
            'keith': 1600,
            'erica': 1158,
            'george': 1300
        }
    ],
    'type' : 'column',
    'title' : 'economic',
    'description': 'GDP and Debt in 2010',
    'series' : [
        {
          'dataField' : 'keith',
          'displayText' : 'keith'
        },
        {
          'dataField' : 'erica',
          'displayText' : 'erica'
        },
        {
          'dataField' : 'george',
          'displayText' : 'george'
        }
    ],
    'datafields' : [
      {
        'name' : 'Day',
      },
      {
        'name' : 'keith',
      },
      {
        'name' : 'erica',
      },
      {
        'name' : 'george',
      }
    ]
  };

  if (this.val === 'kind') {
    Data = {
      'data': [
        {
          'Day': 1,
          'keith': 1350,
          'erica': 1158,
          'george': 1201
        },
        {
            'Day': 2,
            'keith': 1331,
            'erica': 1100,
            'george': 1200
        },
        {
            'Day': 3,
            'keith': 1391,
            'erica': 1000,
            'george': 1220
        },
        {
            'Day': 4,
            'keith': 1391,
            'erica': 1000,
            'george': 1400
        },
        {
            'Day': 5,
            'keith': 1391,
            'erica': 1700,
            'george': 2000
        },
        {
            'Day': 6,
            'keith': 1791,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 7,
            'keith': 1600,
            'erica': 1158,
            'george': 1300
        }
      ],
      'type' : 'line',
      'title' : '라인 차트 입니다.',
      'description': 'GDP and Debt in 2010',
      'series' : [
          {
            'dataField' : 'keith',
            'displayText' : 'keith'
          },
          {
            'dataField' : 'erica',
            'displayText' : 'erica'
          },
          {
            'dataField' : 'george',
            'displayText' : 'george'
          }
      ],
      'datafields' : [
        {
          'name' : 'Day',
        },
        {
          'name' : 'keith',
        },
        {
          'name' : 'erica',
        },
        {
          'name' : 'george',
        }
      ]
    };

  } else if (this.val === 'data') {
    Data = {
      'data': [
          {
              'Day': 1,
              'keith': 1000,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 2,
              'keith': 500,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 3,
              'keith': 800,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 4,
              'keith': 1391,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 5,
              'keith': 1391,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 6,
              'keith': 1391,
              'erica': 1158,
              'george': 1201
          },
          {
              'Day': 7,
              'keith': 1391,
              'erica': 1158,
              'george': 1300
          }
      ],
      'type' : 'column',
      'title' : 'economic',
      'description': 'GDP and Debt in 2010',
      'series' : [
          {
            'dataField' : 'keith',
            'displayText' : 'keith'
          },
          {
            'dataField' : 'erica',
            'displayText' : 'erica'
          },
          {
            'dataField' : 'george',
            'displayText' : 'george'
          }
      ],
      'datafields' : [
        {
          'name' : 'Day',
        },
        {
          'name' : 'keith',
        },
        {
          'name' : 'erica',
        },
        {
          'name' : 'george',
        }
      ]
    };

  } else if (this.val === 'title') {
    Data = {
      'data': [
        {
          'Day': 1,
          'keith': 1350,
          'erica': 1158,
          'george': 1201
        },
        {
            'Day': 2,
            'keith': 1331,
            'erica': 1100,
            'george': 1200
        },
        {
            'Day': 3,
            'keith': 1391,
            'erica': 1000,
            'george': 1220
        },
        {
            'Day': 4,
            'keith': 1391,
            'erica': 1000,
            'george': 1400
        },
        {
            'Day': 5,
            'keith': 1391,
            'erica': 1700,
            'george': 2000
        },
        {
            'Day': 6,
            'keith': 1791,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 7,
            'keith': 1600,
            'erica': 1158,
            'george': 1300
        }
      ],
      'type' : 'column',
      'title' : '컬럼 차트 입니다.',
      'description': 'hani',
      'series' : [
          {
            'dataField' : 'keith',
            'displayText' : 'keith'
          },
          {
            'dataField' : 'erica',
            'displayText' : 'erica'
          },
          {
            'dataField' : 'george',
            'displayText' : 'george'
          }
      ],
      'datafields' : [
        {
          'name' : 'Day',
        },
        {
          'name' : 'keith',
        },
        {
          'name' : 'erica',
        },
        {
          'name' : 'george',
        }
      ]
  };

  } else if (this.val === 'tint') {
    Data = {
      'data': [
        {
          'Day': 1,
          'keith': 1350,
          'erica': 1158,
          'george': 1201
        },
        {
            'Day': 2,
            'keith': 1331,
            'erica': 1100,
            'george': 1200
        },
        {
            'Day': 3,
            'keith': 1391,
            'erica': 1000,
            'george': 1220
        },
        {
            'Day': 4,
            'keith': 1391,
            'erica': 1000,
            'george': 1400
        },
        {
            'Day': 5,
            'keith': 1391,
            'erica': 1700,
            'george': 2000
        },
        {
            'Day': 6,
            'keith': 1791,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 7,
            'keith': 1600,
            'erica': 1158,
            'george': 1300
        }
      ],
      'type' : 'column',
      'title' : 'economic',
      'description': 'GDP and Debt in 2010',
      'series' : [
          {
            'dataField' : 'keith',
            'displayText' : 'HaNi'
          },
          {
            'dataField' : 'erica',
            'displayText' : 'SungCul'
          },
          {
            'dataField' : 'george',
            'displayText' : 'SangMu'
          }
      ],
      'datafields' : [
        {
          'name' : 'Day',
        },
        {
          'name' : 'keith',
        },
        {
          'name' : 'erica',
        },
        {
          'name' : 'george',
        }
      ]
    };
  }else if (this.val === 'dafault') {
    Data = {
      'data': [
        {
          'Day': 1,
          'keith': 1350,
          'erica': 1158,
          'george': 1201
        },
        {
            'Day': 2,
            'keith': 1331,
            'erica': 1100,
            'george': 1200
        },
        {
            'Day': 3,
            'keith': 1391,
            'erica': 1000,
            'george': 1220
        },
        {
            'Day': 4,
            'keith': 1391,
            'erica': 1000,
            'george': 1400
        },
        {
            'Day': 5,
            'keith': 1391,
            'erica': 1700,
            'george':  2000
        },
        {
            'Day': 6,
            'keith': 1791,
            'erica': 1158,
            'george': 1201
        },
        {
            'Day': 7,
            'keith': 1600,
            'erica': 1158,
            'george': 1300
        }
      ],
      'type' : 'column',
      'title' : 'economic',
      'description': 'GDP and Debt in 2010',
      'series' : [
          {
            'dataField' : 'keith',
            'displayText' : 'keith'
          },
          {
            'dataField' : 'erica',
            'displayText' : 'erica'
          },
          {
            'dataField' : 'george',
            'displayText' : 'george'
          }
      ],
      'datafields' : [
        {
          'name' : 'Day',
        },
        {
          'name' : 'keith',
        },
        {
          'name' : 'erica',
        },
        {
          'name' : 'george',
        }
      ]
    };
  }
    const source = {
          datatype: 'json',
          datafields: [
              { name: 'Day' },
              { name: 'keith' },
              { name: 'erica' },
              { name: 'george' }
          ],
          localdata : Data
    };
    const dataAdapter = new $.jqx.dataAdapter(source, {
      autoBind: true,
       beforeLoadComplete: function(records)
          {
              const sampleData = [];
              const sampleLength =  Object(records).length;
              for (let i = 0; i < sampleLength; i++) {
                  let k = {};
                  k = records[i];
                  sampleData.push(k);
              }
               return sampleData;
          }
      });

    const settings = {
      title: Data.title,
      description: Data.description,
      showLegend: true,
      enableAnimations: true,
      padding: { left: 5, top: 5, right: 5, bottom: 5 },
      titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
      source: dataAdapter,
      xAxis:
      {
        dataField: 'Day',
        gridLines: { visible: true },
        valuesOnTicks: false
      },
      colorScheme: 'scheme01',
      columnSeriesOverlap: false,
      seriesGroups:
      [
        {
          type: Data.type,
          valueAxis:
          {
            visible: true,
            unitInterval: 100,
            title: { text: 'GDP & Debt per Capita($)<br>' }
          },
          series: []
        }
      ]
    };

    for (let i = 0; i < Data.series.length; i++) {
      settings.seriesGroups[0].series.push({
        dataField: Data.series[i]['dataField'],
        displayText: Data.series[i]['displayText']
      });
    }
    // chart 설정 불러오기
    $('#chartContainer').jqxChart(settings);
  }

  jqpiechart() {
    this.val = $('#chart2 option:selected').val();
    console.log(this.val);

    let Data = {
      'data': [
          {
            'Browser' : 'Chrome',
            'Share': 45.6,
          },
          {
            'Browser' : 'IE',
            'Share': 24.6,
          },
          {
            'Browser' : 'Firefox',
            'Share': 20.4,
          },
          {
            'Browser' : 'Safari',
            'Share': 5.1,
          },
          {
            'Browser' : 'Opera',
            'Share': 1.3,
          },
          {
            'Browser' : 'Other',
            'Share': 3.0,
          }
      ],
      'type' : 'pie',
      'title' : 'Desktop browsers share',
      'description': '(source: wikipedia.org)',
      'showLegend' : true,
      'colorScheme' : 'scheme30',
      'series' : [
          {
            'dataField' : 'Share',
            'displayText' : 'Browser',
            'labelRadius' : 120,
            'innerRadius' : null,
            'initialAngle' : 15,
            'radius' : 170,
            'centerOffset' : 0
          }
      ]
    };
    if (this.val === 'dafault') {
      Data = {
        'data': [
            {
              'Browser' : 'Chrome',
              'Share': 45.6,
            },
            {
              'Browser' : 'IE',
              'Share': 24.6,
            },
            {
              'Browser' : 'Firefox',
              'Share': 20.4,
            },
            {
              'Browser' : 'Safari',
              'Share': 5.1,
            },
            {
              'Browser' : 'Opera',
              'Share': 1.3,
            },
            {
              'Browser' : 'Other',
              'Share': 3.0,
            }
        ],
        'type' : 'pie',
        'title' : 'Desktop browsers share',
        'description': '(source: wikipedia.org)',
        'showLegend' : true,
        'colorScheme' : 'scheme30',
        'series' : [
            {
              'dataField' : 'Share',
              'displayText' : 'Browser',
              'labelRadius' : 120,
              'innerRadius' : null,
              'initialAngle' : 15,
              'radius' : 170,
              'centerOffset' : 0
            }
        ]
      };
    }else if (this.val === 'kind') {
      Data = {
        'data': [
            {
              'Browser' : 'Chrome',
              'Share': 45.6,
            },
            {
              'Browser' : 'IE',
              'Share': 24.6,
            },
            {
              'Browser' : 'Firefox',
              'Share': 20.4,
            },
            {
              'Browser' : 'Safari',
              'Share': 5.1,
            },
            {
              'Browser' : 'Opera',
              'Share': 1.3,
            },
            {
              'Browser' : 'Other',
              'Share': 3.0,
            }
        ],
        'type' : 'donut',
        'title' : '도넛 차트 입니다.',
        'description': 'hani',
        'showLegend' : true,
        'colorScheme' : 'scheme07',
        'series' : [
            {
              'dataField' : 'Share',
              'displayText' : 'Browser',
              'labelRadius' : 120,
              'innerRadius' : 70,
              'initialAngle' : 15,
              'radius' : 170,
              'centerOffset' : 0
            }
        ]
      };
    }else if (this.val === 'tint') {
      Data = {
        'data': [
            {
              'Browser' : 'Chrome',
              'Share': 45.6,
            },
            {
              'Browser' : 'IE',
              'Share': 24.6,
            },
            {
              'Browser' : 'Firefox',
              'Share': 20.4,
            },
            {
              'Browser' : 'Safari',
              'Share': 5.1,
            },
            {
              'Browser' : 'Opera',
              'Share': 1.3,
            },
            {
              'Browser' : 'Other',
              'Share': 3.0,
            }
        ],
        'type' : 'pie',
        'title' : '파이 차트 입니다.',
        'description': 'hani',
        'showLegend' : false,
        'colorScheme' : 'scheme30',
        'series' : [
            {
              'dataField' : 'Share',
              'displayText' : 'Browser',
              'labelRadius' : 120,
              'innerRadius' : null,
              'initialAngle' : 15,
              'radius' : 170,
              'centerOffset' : 0
            }
        ]
      };
    }else if (this.val === 'data') {
      Data = {
        'data': [
            {
              'Browser' : 'Chrome',
              'Share': 25.6,
            },
            {
              'Browser' : 'IE',
              'Share': 44.6,
            },
            {
              'Browser' : 'Firefox',
              'Share': 10.4,
            },
            {
              'Browser' : 'Safari',
              'Share': 15.1,
            },
            {
              'Browser' : 'Opera',
              'Share': 1.3,
            },
            {
              'Browser' : 'Other',
              'Share': 3.0,
            }
        ],
        'type' : 'pie',
        'title' : '파이 차트 입니다.',
        'description': 'hani',
        'showLegend' : true,
        'colorScheme' : 'scheme30',
        'series' : [
            {
              'dataField' : 'Share',
              'displayText' : 'Browser',
              'labelRadius' : 120,
              'innerRadius' : null,
              'initialAngle' : 15,
              'radius' : 170,
              'centerOffset' : 0
            }
        ]
      };
    }
    const source = {
      datatype: 'json',
      datafields: [
          { name: 'Browser' },
          { name: 'Share' }
      ],
      localdata : Data
    };
    const dataAdapter = new $.jqx.dataAdapter(source, {
      autoBind: true,
      beforeLoadComplete: function(records)
      {
          const sampleData = [];
          const sampleLength =  Object(records).length;
          for (let i = 0; i < sampleLength; i++) {
              let k = {};
              k = records[i];
              sampleData.push(k);
          }
          return sampleData;
      }
    });

    const settings = {
      title: Data.title,
      description: Data.description,
      enableAnimations: true,
      showLegend: Data.showLegend,
      showBorderLine: true,
      legendPosition: { left: 520, top: 140, width: 100, height: 100 },
      padding: { left: 5, top: 5, right: 5, bottom: 5 },
      titlePadding: { left: 0, top: 0, right: 0, bottom: 10 },
      source: dataAdapter,
      colorScheme: Data.colorScheme,
      seriesGroups:
      [
        {
          type: Data.type,
          showLabels: true,
          series:
            [
                Data.series[0]
            ]
        }
      ]
    };
      $('#chartContainer2').jqxChart(settings);
  }

  jqareachart() {
    const source = {
        datatype: 'tab',
        datafields: [
            { name: 'Date' },
            { name: 'Referral' },
            { name: 'SearchPaid' },
            { name: 'SearchNonPaid' }
        ],
        url: '../../assets/video/website_analytics.txt'
    };
    // tslint:disable-next-line:max-line-length
    const dataAdapter = new $.jqx.dataAdapter(source, { async: false, autoBind: true, loadError: function (xhr, status, error) { alert('Error loading "' + source.url + '" : ' + error); } });

    const settings = {
      title : 'areachart',
      enableAnimations: true,
      showLegend: true,
      padding: { left: 10 , top: 5, right: 10, bottom: 5},
      // titlepadding: { left: 10, top: 5, right: 0, bottom: 10},
      source : dataAdapter,
      xAxis:
      {
        dataField: 'Date',
        type: 'date',
        baseUnit: 'day',
        valuesOnTicks : true,
        labels:
        {
          formatFunction: function(value){
            return value.getDate();
          }
        },
        tooTipFormatFunction: function (value) {
          const months = ['jan' , 'feb', 'mar', 'Apr'  , 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
          return value.getDate() + '-' + months[value.getMonth()] + '-' + value.getFullYear();
        },
      },
      valueAxis:
      {
        unitInterval: 500,
        minvalue: 0,
        maxValue: 4500,
        title: {text: 'Daily visitors by source'}
      },
      colorScheme : 'scheme03',
      seriesGroups :
        [
          {
            type: 'stackedarea',
            series: [
                { dataField: 'SearchNonPaid', displayText : 'Desktop Search' } ,
                { dataField: 'SearchPaid', displayText: 'Mobile Search' },
                { dataField: 'Referral', displayText: 'Social media' }
            ]
          }
        ]
    };
    $('#chartContainer3').jqxChart(settings);
  }
}
