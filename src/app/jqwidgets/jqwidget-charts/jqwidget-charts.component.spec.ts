import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqwidgetChartsComponent } from './jqwidget-charts.component';

describe('JqwidgetChartsComponent', () => {
  let component: JqwidgetChartsComponent;
  let fixture: ComponentFixture<JqwidgetChartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqwidgetChartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqwidgetChartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
