import { TestBed, inject } from '@angular/core/testing';

import { JqwidgetChartService } from './jqwidget-chart.service';

describe('JqwidgetChartService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JqwidgetChartService]
    });
  });

  it('should be created', inject([JqwidgetChartService], (service: JqwidgetChartService) => {
    expect(service).toBeTruthy();
  }));
});
