import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqwidgetWithComponent } from './jqwidget-with.component';

describe('JqwidgetWithComponent', () => {
  let component: JqwidgetWithComponent;
  let fixture: ComponentFixture<JqwidgetWithComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqwidgetWithComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqwidgetWithComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
