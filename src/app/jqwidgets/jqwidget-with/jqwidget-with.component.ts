import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-jqwidget-with',
  templateUrl: './jqwidget-with.component.html',
  styleUrls: ['./jqwidget-with.component.css']
})
export class JqwidgetWithComponent implements OnInit {

  constructor() { }
  sampleData_list = [];
  series_list = [];
  datafields_list = [];
  columns_list = [];
  selectValue:any;
  selectMax:any;
  maxValue:any;
  intervalValue:any;
  titleValue:any;
  ngOnInit() {
  this.getwidth();
  }


  getwidth(){

    // max 값 설정
    this.selectMax = $('#chartMaxSelect option:selected').val();
    console.log(this.selectMax);
    this.intervalValue = 2000;
    this.maxValue = 10000;
    if(this.selectMax == 1000){
      this.intervalValue = 200;
      this.maxValue = 1000;
    }



    // 데이터 변경 설정
    this.selectValue = $('#gridSelect option:selected').val();
    console.log(this.selectValue);


    // Grid Data
    this.sampleData_list = [    { "serialNumber": "e1003", "cameraName": "SNP-5300","ip": 30, "cameraStatus": "재생", "Service": 900, "playSet": "on" },
      { "serialNumber": "e10032", "cameraName": "SND-5011","ip": 65, "cameraStatus": "철거", "Service": 800, "playSet": "off" },
      { "serialNumber": "e220031", "cameraName": "SNB-6004","ip": 70, "cameraStatus": "재생", "Service": 440, "playSet": "off" },
      { "serialNumber": "e431003", "cameraName": "SND-5001","ip": 30, "cameraStatus": "고장", "Service": 130, "playSet": "off" },
      { "serialNumber": "e14203", "cameraName": "SNF-8010","ip": 51, "cameraStatus": "정지", "Service": 720, "playSet": "off" },
      { "serialNumber": "e10103", "cameraName": "SNV-5080","ip": 34, "cameraStatus": "재생", "Service": 660, "playSet": "off" },
      { "serialNumber": "e65003", "cameraName": "SNO-6084RN","ip": 56, "cameraStatus": "철거", "Service": 640, "playSet": "off" }
    ];
    // Series
    this.series_list =     [
      { dataField: 'ip', displayText: 'ip' },
      { dataField: 'Service', displayText: 'Service' }
    ];
    // datafields
    this.datafields_list = [
      { name: "serialNumber", type: "string" },
      { name: "cameraName", type: "string" },
      { name: "ip", type: "number" },
      { name: "cameraStatus", type: "string" },
      { name: "Service", type: "number" },
      { name: "playSet", type: "string" }
    ];
    // Columns
    this.columns_list =    [
      { text: "시리얼넘버", width: '30%', datafield: "serialNumber", filteritems: ["e10032", "e220031", "e14203", "e65003", "e1003", "e14203", "e431003"], filtertype: "checkedlist" },
      { text: "카메라명", width: '10%', datafield: "cameraName" },
      { text: "카메라IP", width: '10%', datafield: "ip" },
      { text: "카메라상태", width: '10%', datafield: "cameraStatus" },
      { text: "서비스", width: '20%', datafield: "Service" },
      { text: "영상보기", width: '20%', datafield: "playSet" }
    ];






    if (this.selectValue === 'data'){
      // Grid Data
      this.sampleData_list = [    { "User": "SK인포섹", "Channel": "60","Vas": 3, "Remote": "x", "Cost": 900, "Payment": 1900 },
        { "User": "구로구청", "Channel": "30","Vas": 3, "Remote": "o", "Cost": 800, "Payment": 900 },
        { "User": "인천시청", "Channel": "50","Vas": 3, "Remote": "x", "Cost": 440, "Payment": 900 },
        { "User": "제주도청", "Channel": "10","Vas": 3, "Remote": "o", "Cost": 130, "Payment": 900 },
        { "User": "SK하이닉스", "Channel": "70","Vas": 3, "Remote": "o", "Cost": 720, "Payment": 900 },
        { "User": "SKTelecom", "Channel": "30","Vas": 3, "Remote": "o", "Cost": 660, "Payment": 900 },
        { "User": "Innodep", "Channel": "90","Vas": 3, "Remote": "x", "Cost": 640, "Payment": 900 }
      ];
      // Series
      this.series_list =     [
        { dataField: 'Channel', displayText: 'Channel' },
        { dataField: 'Cost', displayText: 'Cost' },
        { dataField: 'Payment', displayText: 'Payment' }
      ];
      // datafields
      this.datafields_list = [
        { name: "User", type: "string" },
        { name: "Vas", type: "number" },
        { name: "Remote", type: "string" },
        { name: "Channel", type: "string" },
        { name: "Cost", type: "number" },
        { name: "Payment", type: "number" }
      ];
      // Columns
      this.columns_list =    [
        { text: "사용자", width: '30%', datafield: "User", filteritems: ["SK인포섹", "구로구청", "인천시청", "제주도청", "SK하이닉스", "SKTelecom", "Innodep"], filtertype: "checkedlist" },
        { text: "채널", width: '10%', datafield: "Channel" },
        { text: "VAS", width: '10%', datafield: "Vas" },
        { text: "원격관제", width: '10%', datafield: "Remote" },
        { text: "예상 비용", width: '20%', datafield: "Cost" },
        { text: "예상 청구 금액", width: '20%', datafield: "Payment" }
      ];

    }

    // grid 데이터 부분 xAxis에서 선택한 key값이 차트의 데이터가 됨
    let sampleData = this.sampleData_list;

    // 차트 그리는 부분
    let settings = {
      title: this.titleValue,
      description: true,
      enableAnimations: true,
      showLegend: true,
      padding: { left: 5, top: 5, right: 5, bottom: 5 },
      titlePadding: { left: 90, top: 0, right: 0, bottom: 10 },
      source: sampleData,
      // x 축 DATA 값
      xAxis:
        {
          dataField: 'User',
          gridLines: { visible: true }
        },
      // 차트 바 색상
      colorScheme: 'scheme02',
      seriesGroups:
        [
          {
            type: 'column',
            columnsGapPercent: 50,
            seriesGapPercent: 0,
            // Y축 Data값
            valueAxis:
              {
                visible: true,
                unitInterval: this.intervalValue,
                minValue: 0,
                maxValue: this.maxValue,
                title: { text: 'Time in minutes' }
              },
            // 차트에 표시되는 Data
            series: this.series_list
          }
        ]
    };
    // 차트에 데이터 적용
    $('#jqxChart').jqxChart(settings);
    var adapter = new $.jqx.dataAdapter({
      datafields: this.datafields_list,
      localdata: sampleData,
      datatype: 'array'
    });
    $("#jqxGrid").jqxGrid({
      width: 848,
      height: 232,
      filterable: true,
      showfilterrow: true,
      source: adapter,
      columns: this.columns_list

    });
    $("#jqxGrid").on('filter', function () {
      var rows = $("#jqxGrid").jqxGrid('getrows');
      var chart = $('#jqxChart').jqxChart('getInstance');
      chart.source = rows;
      chart.update();
    });

    // 옵션 - filter 처리 On/Off
    $("#jqxCheckBox_filter").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_filter").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_filter").find('span')[1].innerHTML = 'filter On';
        $('#jqxGrid').jqxGrid({ filterable:true});

      }
      else {
        $("#jqxCheckBox_filter").find('span')[1].innerHTML = 'filter Off';
        $('#jqxGrid').jqxGrid({filterable:false});

      }
    });

    // 옵션 - filter 처리 On/Off
    $("#jqxCheckBox_filter").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_filter").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_filter").find('span')[1].innerHTML = 'filter On';
        $('#jqxGrid').jqxGrid({ filterable:true});

      }
      else {
        $("#jqxCheckBox_filter").find('span')[1].innerHTML = 'filter Off';
        $('#jqxGrid').jqxGrid({filterable:false});

      }
    });

    // 옵션 - Title 처리 On/Off
    $("#jqxCheckBox_title").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_title").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_title").find('span')[1].innerHTML = 'Title On';

        settings.title = "서비스현황관리";
        //$('#jqxGrid').jqxGrid({ title:"서비스 현황 관리"});
        $('#jqxChart').jqxChart(settings);

        console.log(settings);
        console.log(settings.title);

      }
      else {
        $("#jqxCheckBox_title").find('span')[1].innerHTML = 'Title Off';
        settings.title = false;
        $('#jqxChart').jqxChart(settings);
        //console.log($('#jqxChart').jqxChart(settings));
        console.log(settings);

      }
    });

    // 옵션 - ToolTip 처리 On/Off
    $("#jqxCheckBox_tooltip").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_tooltip").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_tooltip").find('span')[1].innerHTML = 'Tooltips On';
        $('#jqxGrid').jqxGrid({ showToolTips:true});

      }
      else {
        $("#jqxCheckBox_tooltip").find('span')[1].innerHTML = 'Tooltips Off';
        $('#jqxGrid').jqxGrid({showToolTips:false});

      }
    });





  }

}
