import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqwidgetTreeComponent } from './jqwidget-tree.component';

describe('JqwidgetTreeComponent', () => {
  let component: JqwidgetTreeComponent;
  let fixture: ComponentFixture<JqwidgetTreeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqwidgetTreeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqwidgetTreeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
