import { Component, OnInit } from '@angular/core';

declare var $;

@Component({
  selector: 'app-jqwidget-tree',
  templateUrl: './jqwidget-tree.component.html',
  styleUrls: ['./jqwidget-tree.component.css']
})
export class JqwidgetTreeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    this.jqtree();
  }

  jqtree() {

    $('#treeA').jqxTree({ height: '450px', width: '300px', incrementalSearch: true});
    $('#Add').jqxButton({ height: '25px', width: '100px'});
    $('#Remove').jqxButton({ height: '25px', width: '100px'});
    $('#treeA').css('visibility', 'visible');

    // 검색
    $('#searchbtn').click( () => {
      $('#treeA').jqxTree('collapseAll');

      const searchItem = $('#search').val();
      const items = $('#treeA').jqxTree('getItems');

        for ( let i = 0; i < items.length; i++) {
          if (searchItem === items[i].label.toLowerCase()) {
             console.log(items[i]);
            $('#treeA').jqxTree('selectItem', items[i]);
            $('#treeA').jqxTree('expandItem', items[i]);
          }
        }
    });

    // 우클릭
    const contextMenu = $('#jqxMenu').jqxMenu({ width: '120px', height: '56px', autoOpenPopup: false, mode: 'popup'});
    const clickItem = null;

    const attachContextMenu = () => {
      $('#treeA li').on('mousedown', (event) => {
        const target = $(event.target).parents('li:first')[0];
        const rightClick = isRightClick(event);
        if (rightClick && target !== null) {
          $('#treeA').jqxTree('selectedItem', target);
          const scrollTop = $(window).scrollTop();
          const scrollLeft = $(window).scrollLeft();
          contextMenu.jqxMenu('open', parseInt(event.clientX, 10 ) + 5 + scrollLeft, parseInt(event.clientY, 10 ) + scrollTop);
          return false;
        }
      });
    };
    attachContextMenu();

    $('#jqxMenu').on('itemclick', function (event) {
      const item = $.trim($(event.args).text());
      switch (item) {
          case 'Add Item':
              let selectedItem = $('#treeA').jqxTree('selectedItem');
              if (selectedItem !== null) {
                  $('#treeA').jqxTree('addTo', { label: 'Item' }, selectedItem);
                  attachContextMenu();
              }
              break;
          case 'Remove Item':
              selectedItem = $('#treeA').jqxTree('selectedItem');
              if (selectedItem !== null) {
                  $('#treeA').jqxTree('removeItem', selectedItem);
                  attachContextMenu();
              }
              break;
      }
    });
    // // // disable the default browser's context menu.
    $(document).on('contextmenu', function (e) {
        if ($(e.target).parents('.jqx-tree').length > 0) {
            return false;
        }
        return true;
    });
    function isRightClick(event) {
        let rightclick;
        if (!event) {event = window.event; }
        if (event.which) {rightclick = (event.which === 3); }else if (event.button) {rightclick = (event.button === 2); }
        return rightclick;
    }

    // 트리 추가
    $('#Add').click(() => {

       const selectedItem = $('#treeA').jqxTree('selectedItem');
        if (selectedItem !== null) {
          $('#treeA').jqxTree('addTo', {label: 'item'}, selectedItem);
          $('#treeA').jqxTree('render');
        }else {
          $('#treeA').jqxtree('addTo', {label: 'item'}, null, false);
          $('#treeA').jqxtree('render');
       }

    });

    // 트리 삭제
    $('#Remove').jqxButton({ height: '25px', width: '100px'});
    $('#Remove').click( () => {

      const selectedItem = $('#treeA').jqxTree('selectedItem');
      $('#treeA').jqxTree('removeItem', selectedItem, false);
      $('#treeA').jqxTree('render');

    });

    // 트리 드래그 이동
    $('#treeA').jqxTree({ allowDrag: true, allowDrop: true, height: '300px', width: '220px',
      dragStart: (item) => {
        // 원하는 트리 고정
        // if (item.label === 'item') { return false; }
      }
    });
    $('#treeB').jqxTree({ allowDrag: true, allowDrop: true, height: '300px', width: '220px',
      dragEnd: (item, dropItem, args, dropPosition, tree) => {
        // if (item.label === 'Forum') { return false; }
      }
    });
    $('#treeA').css('visibility', 'visible');
    $('#treeB').css('visibility', 'visible');

    $('#treeA, #treeB').on('dragStart', function (event) {

    });
    $('#treeA, #treeB').on('dragEnd', function (event) {

        if (event.args.label) {
            const ev = event.args.originalEvent;
            let x = ev.pageX;
            let y = ev.pageY;
            if (event.args.originalEvent && event.args.originalEvent.originalEvent && event.args.originalEvent.originalEvent.touches) {
               const touch = event.args.originalEvent.originalEvent.changedTouches[0];
               x = touch.pageX;
               y = touch.pageY;
            }

            const offset = $('#textarea').offset();
            const width = $('textarea').width();
            const height = $('textarea').height();
            const right = parseInt(offset.left, 10) + width;
            const bottom = parseInt(offset.top, 10) + height;
            if (x >= parseInt(offset.left, 10) && x <= right) {
              if ( y >= parseInt(offset.top, 10) && y <= bottom) {
                  $('#textarea').val(event.args.label);
              }
            }
        }
    });
  }

}
