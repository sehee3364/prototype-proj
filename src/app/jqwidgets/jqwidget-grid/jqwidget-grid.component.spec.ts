import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JqwidgetGridComponent } from './jqwidget-grid.component';

describe('JqwidgetGridComponent', () => {
  let component: JqwidgetGridComponent;
  let fixture: ComponentFixture<JqwidgetGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JqwidgetGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JqwidgetGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
