import {Component, OnInit, OnChanges, SimpleChanges, Input} from '@angular/core';
import {isBoolean} from "util";

@Component({
  selector: 'app-jqwidget-grid',
  templateUrl: './jqwidget-grid.component.html',
  styleUrls: ['./jqwidget-grid.component.css']
})
export class JqwidgetGridComponent implements OnInit, OnChanges {
@Input() paging:number;
@Input() paging_options:boolean;

  constructor() { }

  data = new Array();
  paging_option = true;
  label: any;
  pagerrenderer:any;
  checkbox_option:string;
  selectValue:any;
  select_datafields_list:any;
  select_data_list:any;
  columns_list:any;

  fristname_data_list         = [ "Andrew", "Nancy", "Shelley", "Regina", "Yoshi", "Antoni", "Mayumi", "Ian", "Peter", "Lars", "Petra", "Martin", "Sven", "Elio", "Beate", "Cheryl", "Michael", "Guylene" ];
  lastname_data_list          = [ "Fuller", "Davolio", "Burke", "Murphy", "Nagase", "Saavedra", "Ohno", "Devling", "Wilson", "Peterson", "Winkler", "Bein", "Petersen", "Rossi", "Vileid", "Saylor", "Bjorn", "Nodier" ];
  productname_data_list       = [ "Black Tea", "Green Tea", "Caffe Espresso", "Doubleshot Espresso", "Caffe Latte", "White Chocolate Mocha", "Cramel Latte", "Caffe Americano", "Cappuccino", "Espresso Truffle", "Espresso con Panna", "Peppermint Mocha Twist" ];
  pricevalue_data_list        = [ "2.25", "1.5", "3.0", "3.3", "4.5", "3.6", "3.8", "2.5", "5.0", "1.75", "3.25", "4.0" ];
  columns_data_list           = [
    { text: '이름', datafield: 'firstname', width: 100 },
    { text: '성', datafield: 'lastname', width: 100 },
    { text: '물품', datafield: 'productname', width: 180 },
    { text: '수량', datafield: 'quantity', width: 80, cellsalign: 'right' },
    { text: '가격', datafield: 'price', width: 90, cellsalign: 'right', cellsformat: 'c2' },
    { text: '합계', datafield: 'total', width: 100, cellsalign: 'right', cellsformat: 'c2' }
  ]


  ngOnInit() {
    this.getGrid();
  }

  ngOnChanges(changes: SimpleChanges){
  this.getGrid();
  }


// grid 호출 함수
  getGrid(){

    this.selectValue = $('#gridSelect option:selected').val();

    this.select_data_list = [
        {
          "CompanyName":"Alfreds Futterkiste",
          "ContactName":"Maria Anders",
          "ContactTitle":"Sales Representative",
          "Address":"Obere Str. 57",
          "City":"Berlin",
          "Country":"Germany"
        },
      {
        "CompanyName":"Ana Trujillo Emparedados y helados",
        "ContactName":"Ana Trujillo",
        "ContactTitle":"Owner",
        "Address":"Avda. de la Constitucin 2222",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Antonio Moreno Taquera",
        "ContactName":"Antonio Moreno",
        "ContactTitle":"Owner",
        "Address":"Mataderos 2312",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Around the Horn",
        "ContactName":"Thomas Hardy",
        "ContactTitle":"Sales Representative",
        "Address":"120 Hanover Sq.",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Berglunds snabbkp",
        "ContactName":"Christina Berglund",
        "ContactTitle":"Order Administrator",
        "Address":"Berguvsvgen 8",
        "City":"Lule",
        "Country":"Sweden"
      },
      {
        "CompanyName":"Blauer See Delikatessen",
        "ContactName":"Hanna Moos",
        "ContactTitle":"Sales Representative",
        "Address":"Forsterstr. 57",
        "City":"Mannheim",
        "Country":"Germany"
      },
      {
        "CompanyName":"Blondesddsl pre et fils",
        "ContactName":"Frdrique Citeaux",
        "ContactTitle":"Marketing Manager",
        "Address":"24, place Klber",
        "City":"Strasbourg",
        "Country":"France"
      },
      {
        "CompanyName":"Blido Comidas preparadas",
        "ContactName":"Martn Sommer",
        "ContactTitle":"Owner",
        "Address":"C\/ Araquil, 67",
        "City":"Madrid",
        "Country":"Spain"
      },
      {
        "CompanyName":"Bon app'",
        "ContactName":"Laurence Lebihan",
        "ContactTitle":"Owner",
        "Address":"12, rue des Bouchers",
        "City":"Marseille",
        "Country":"France"
      },
      {
        "CompanyName":"Bottom-Dollar Markets",
        "ContactName":"Elizabeth Lincoln",
        "ContactTitle":"Accounting Manager",
        "Address":"23 Tsawassen Blvd.",
        "City":"Tsawassen",
        "Country":"Canada"
      },
      {
        "CompanyName":"Beverages",
        "ContactName":"Victoria Ashworth",
        "ContactTitle":"Sales Representative",
        "Address":"Fauntleroy Circus",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Cactus Comidas para llevar",
        "ContactName":"Patricio Simpson",
        "ContactTitle":"Sales Agent",
        "Address":"Cerrito 333",
        "City":"Buenos Aires",
        "Country":"Argentina"
      },
      {
        "CompanyName":"Centro comercial Moctezuma",
        "ContactName":"Francisco Chang",
        "ContactTitle":"Marketing Manager",
        "Address":"Sierras de Granada 9993",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Chop-suey Chinese",
        "ContactName":"Yang Wang",
        "ContactTitle":"Owner",
        "Address":"Hauptstr. 29",
        "City":"Bern",
        "Country":"Switzerland"
      },
      {
        "CompanyName":"Comrcio Mineiro",
        "ContactName":"Pedro Afonso",
        "ContactTitle":"Sales Associate",
        "Address":"Av. dos Lusadas, 23",
        "City":"Sao Paulo",
        "Country":"Brazil"
      },
      {
        "CompanyName":"Consolidated Holdings",
        "ContactName":"Elizabeth Brown",
        "ContactTitle":"Sales Representative",
        "Address":"Berkeley Gardens 12 Brewery",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Drachenblut Delikatessen",
        "ContactName":"Sven Ottlieb",
        "ContactTitle":"Order Administrator",
        "Address":"Walserweg 21",
        "City":"Aachen",
        "Country":"Germany"
      },
      {
        "CompanyName":"Du monde entier",
        "ContactName":"Janine Labrune",
        "ContactTitle":"Owner",
        "Address":"67, rue des Cinquante Otages",
        "City":"Nantes",
        "Country":"France"
      },
      {
        "CompanyName":"Eastern Connection",
        "ContactName":"Ann Devon",
        "ContactTitle":"Sales Agent",
        "Address":"35 King George",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Ernst Handel",
        "ContactName":"Roland Mendel",
        "ContactTitle":"Sales Manager",
        "Address":"Kirchgasse 6",
        "City":"Graz",
        "Country":"Austria"
      },
      {
        "CompanyName":"Alfreds Futterkiste",
        "ContactName":"Maria Anders",
        "ContactTitle":"Sales Representative",
        "Address":"Obere Str. 57",
        "City":"Berlin",
        "Country":"Germany"
      },
      {
        "CompanyName":"Ana Trujillo Emparedados y helados",
        "ContactName":"Ana Trujillo",
        "ContactTitle":"Owner",
        "Address":"Avda. de la Constitucin 2222",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Antonio Moreno Taquera",
        "ContactName":"Antonio Moreno",
        "ContactTitle":"Owner",
        "Address":"Mataderos 2312",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Around the Horn",
        "ContactName":"Thomas Hardy",
        "ContactTitle":"Sales Representative",
        "Address":"120 Hanover Sq.",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Berglunds snabbkp",
        "ContactName":"Christina Berglund",
        "ContactTitle":"Order Administrator",
        "Address":"Berguvsvgen 8",
        "City":"Lule",
        "Country":"Sweden"
      },
      {
        "CompanyName":"Blauer See Delikatessen",
        "ContactName":"Hanna Moos",
        "ContactTitle":"Sales Representative",
        "Address":"Forsterstr. 57",
        "City":"Mannheim",
        "Country":"Germany"
      },
      {
        "CompanyName":"Blondesddsl pre et fils",
        "ContactName":"Frdrique Citeaux",
        "ContactTitle":"Marketing Manager",
        "Address":"24, place Klber",
        "City":"Strasbourg",
        "Country":"France"
      },
      {
        "CompanyName":"Blido Comidas preparadas",
        "ContactName":"Martn Sommer",
        "ContactTitle":"Owner",
        "Address":"C\/ Araquil, 67",
        "City":"Madrid",
        "Country":"Spain"
      },
      {
        "CompanyName":"Bon app'",
        "ContactName":"Laurence Lebihan",
        "ContactTitle":"Owner",
        "Address":"12, rue des Bouchers",
        "City":"Marseille",
        "Country":"France"
      },
      {
        "CompanyName":"Bottom-Dollar Markets",
        "ContactName":"Elizabeth Lincoln",
        "ContactTitle":"Accounting Manager",
        "Address":"23 Tsawassen Blvd.",
        "City":"Tsawassen",
        "Country":"Canada"
      },
      {
        "CompanyName":"Beverages",
        "ContactName":"Victoria Ashworth",
        "ContactTitle":"Sales Representative",
        "Address":"Fauntleroy Circus",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Cactus Comidas para llevar",
        "ContactName":"Patricio Simpson",
        "ContactTitle":"Sales Agent",
        "Address":"Cerrito 333",
        "City":"Buenos Aires",
        "Country":"Argentina"
      },
      {
        "CompanyName":"Centro comercial Moctezuma",
        "ContactName":"Francisco Chang",
        "ContactTitle":"Marketing Manager",
        "Address":"Sierras de Granada 9993",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Chop-suey Chinese",
        "ContactName":"Yang Wang",
        "ContactTitle":"Owner",
        "Address":"Hauptstr. 29",
        "City":"Bern",
        "Country":"Switzerland"
      },
      {
        "CompanyName":"Comrcio Mineiro",
        "ContactName":"Pedro Afonso",
        "ContactTitle":"Sales Associate",
        "Address":"Av. dos Lusadas, 23",
        "City":"Sao Paulo",
        "Country":"Brazil"
      },
      {
        "CompanyName":"Consolidated Holdings",
        "ContactName":"Elizabeth Brown",
        "ContactTitle":"Sales Representative",
        "Address":"Berkeley Gardens 12 Brewery",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Drachenblut Delikatessen",
        "ContactName":"Sven Ottlieb",
        "ContactTitle":"Order Administrator",
        "Address":"Walserweg 21",
        "City":"Aachen",
        "Country":"Germany"
      },
      {
        "CompanyName":"Du monde entier",
        "ContactName":"Janine Labrune",
        "ContactTitle":"Owner",
        "Address":"67, rue des Cinquante Otages",
        "City":"Nantes",
        "Country":"France"
      },
      {
        "CompanyName":"Eastern Connection",
        "ContactName":"Ann Devon",
        "ContactTitle":"Sales Agent",
        "Address":"35 King George",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Ernst Handel",
        "ContactName":"Roland Mendel",
        "ContactTitle":"Sales Manager",
        "Address":"Kirchgasse 6",
        "City":"Graz",
        "Country":"Austria"
      },


      {
        "CompanyName":"Alfreds Futterkiste",
        "ContactName":"Maria Anders",
        "ContactTitle":"Sales Representative",
        "Address":"Obere Str. 57",
        "City":"Berlin",
        "Country":"Germany"
      },
      {
        "CompanyName":"Ana Trujillo Emparedados y helados",
        "ContactName":"Ana Trujillo",
        "ContactTitle":"Owner",
        "Address":"Avda. de la Constitucin 2222",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Antonio Moreno Taquera",
        "ContactName":"Antonio Moreno",
        "ContactTitle":"Owner",
        "Address":"Mataderos 2312",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Around the Horn",
        "ContactName":"Thomas Hardy",
        "ContactTitle":"Sales Representative",
        "Address":"120 Hanover Sq.",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Berglunds snabbkp",
        "ContactName":"Christina Berglund",
        "ContactTitle":"Order Administrator",
        "Address":"Berguvsvgen 8",
        "City":"Lule",
        "Country":"Sweden"
      },
      {
        "CompanyName":"Blauer See Delikatessen",
        "ContactName":"Hanna Moos",
        "ContactTitle":"Sales Representative",
        "Address":"Forsterstr. 57",
        "City":"Mannheim",
        "Country":"Germany"
      },
      {
        "CompanyName":"Blondesddsl pre et fils",
        "ContactName":"Frdrique Citeaux",
        "ContactTitle":"Marketing Manager",
        "Address":"24, place Klber",
        "City":"Strasbourg",
        "Country":"France"
      },
      {
        "CompanyName":"Blido Comidas preparadas",
        "ContactName":"Martn Sommer",
        "ContactTitle":"Owner",
        "Address":"C/ Araquil, 67",
        "City":"Madrid",
        "Country":"Spain"
      },
      {
        "CompanyName":"Bon app'",
        "ContactName":"Laurence Lebihan",
        "ContactTitle":"Owner",
        "Address":"12, rue des Bouchers",
        "City":"Marseille",
        "Country":"France"
      },
      {
        "CompanyName":"Bottom-Dollar Markets",
        "ContactName":"Elizabeth Lincoln",
        "ContactTitle":"Accounting Manager",
        "Address":"23 Tsawassen Blvd.",
        "City":"Tsawassen",
        "Country":"Canada"
      },
      {
        "CompanyName":"Beverages",
        "ContactName":"Victoria Ashworth",
        "ContactTitle":"Sales Representative",
        "Address":"Fauntleroy Circus",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Cactus Comidas para llevar",
        "ContactName":"Patricio Simpson",
        "ContactTitle":"Sales Agent",
        "Address":"Cerrito 333",
        "City":"Buenos Aires",
        "Country":"Argentina"
      },
      {
        "CompanyName":"Centro comercial Moctezuma",
        "ContactName":"Francisco Chang",
        "ContactTitle":"Marketing Manager",
        "Address":"Sierras de Granada 9993",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Chop-suey Chinese",
        "ContactName":"Yang Wang",
        "ContactTitle":"Owner",
        "Address":"Hauptstr. 29",
        "City":"Bern",
        "Country":"Switzerland"
      },
      {
        "CompanyName":"Comrcio Mineiro",
        "ContactName":"Pedro Afonso",
        "ContactTitle":"Sales Associate",
        "Address":"Av. dos Lusadas, 23",
        "City":"Sao Paulo",
        "Country":"Brazil"
      },
      {
        "CompanyName":"Consolidated Holdings",
        "ContactName":"Elizabeth Brown",
        "ContactTitle":"Sales Representative",
        "Address":"Berkeley Gardens 12 Brewery",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Drachenblut Delikatessen",
        "ContactName":"Sven Ottlieb",
        "ContactTitle":"Order Administrator",
        "Address":"Walserweg 21",
        "City":"Aachen",
        "Country":"Germany"
      },
      {
        "CompanyName":"Du monde entier",
        "ContactName":"Janine Labrune",
        "ContactTitle":"Owner",
        "Address":"67, rue des Cinquante Otages",
        "City":"Nantes",
        "Country":"France"
      },
      {
        "CompanyName":"Eastern Connection",
        "ContactName":"Ann Devon",
        "ContactTitle":"Sales Agent",
        "Address":"35 King George",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Ernst Handel",
        "ContactName":"Roland Mendel",
        "ContactTitle":"Sales Manager",
        "Address":"Kirchgasse 6",
        "City":"Graz",
        "Country":"Austria"
      },
      {
        "CompanyName":"Alfreds Futterkiste",
        "ContactName":"Maria Anders",
        "ContactTitle":"Sales Representative",
        "Address":"Obere Str. 57",
        "City":"Berlin",
        "Country":"Germany"
      },
      {
        "CompanyName":"Ana Trujillo Emparedados y helados",
        "ContactName":"Ana Trujillo",
        "ContactTitle":"Owner",
        "Address":"Avda. de la Constitucin 2222",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Antonio Moreno Taquera",
        "ContactName":"Antonio Moreno",
        "ContactTitle":"Owner",
        "Address":"Mataderos 2312",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Around the Horn",
        "ContactName":"Thomas Hardy",
        "ContactTitle":"Sales Representative",
        "Address":"120 Hanover Sq.",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Berglunds snabbkp",
        "ContactName":"Christina Berglund",
        "ContactTitle":"Order Administrator",
        "Address":"Berguvsvgen 8",
        "City":"Lule",
        "Country":"Sweden"
      },
      {
        "CompanyName":"Blauer See Delikatessen",
        "ContactName":"Hanna Moos",
        "ContactTitle":"Sales Representative",
        "Address":"Forsterstr. 57",
        "City":"Mannheim",
        "Country":"Germany"
      },
      {
        "CompanyName":"Blondesddsl pre et fils",
        "ContactName":"Frdrique Citeaux",
        "ContactTitle":"Marketing Manager",
        "Address":"24, place Klber",
        "City":"Strasbourg",
        "Country":"France"
      },
      {
        "CompanyName":"Blido Comidas preparadas",
        "ContactName":"Martn Sommer",
        "ContactTitle":"Owner",
        "Address":"C/ Araquil, 67",
        "City":"Madrid",
        "Country":"Spain"
      },
      {
        "CompanyName":"Bon app'",
        "ContactName":"Laurence Lebihan",
        "ContactTitle":"Owner",
        "Address":"12, rue des Bouchers",
        "City":"Marseille",
        "Country":"France"
      },
      {
        "CompanyName":"Bottom-Dollar Markets",
        "ContactName":"Elizabeth Lincoln",
        "ContactTitle":"Accounting Manager",
        "Address":"23 Tsawassen Blvd.",
        "City":"Tsawassen",
        "Country":"Canada"
      },
      {
        "CompanyName":"Beverages",
        "ContactName":"Victoria Ashworth",
        "ContactTitle":"Sales Representative",
        "Address":"Fauntleroy Circus",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Cactus Comidas para llevar",
        "ContactName":"Patricio Simpson",
        "ContactTitle":"Sales Agent",
        "Address":"Cerrito 333",
        "City":"Buenos Aires",
        "Country":"Argentina"
      },
      {
        "CompanyName":"Centro comercial Moctezuma",
        "ContactName":"Francisco Chang",
        "ContactTitle":"Marketing Manager",
        "Address":"Sierras de Granada 9993",
        "City":"Mxico D.F.",
        "Country":"Mexico"
      },
      {
        "CompanyName":"Chop-suey Chinese",
        "ContactName":"Yang Wang",
        "ContactTitle":"Owner",
        "Address":"Hauptstr. 29",
        "City":"Bern",
        "Country":"Switzerland"
      },
      {
        "CompanyName":"Comrcio Mineiro",
        "ContactName":"Pedro Afonso",
        "ContactTitle":"Sales Associate",
        "Address":"Av. dos Lusadas, 23",
        "City":"Sao Paulo",
        "Country":"Brazil"
      },
      {
        "CompanyName":"Consolidated Holdings",
        "ContactName":"Elizabeth Brown",
        "ContactTitle":"Sales Representative",
        "Address":"Berkeley Gardens 12 Brewery",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Drachenblut Delikatessen",
        "ContactName":"Sven Ottlieb",
        "ContactTitle":"Order Administrator",
        "Address":"Walserweg 21",
        "City":"Aachen",
        "Country":"Germany"
      },
      {
        "CompanyName":"Du monde entier",
        "ContactName":"Janine Labrune",
        "ContactTitle":"Owner",
        "Address":"67, rue des Cinquante Otages",
        "City":"Nantes",
        "Country":"France"
      },
      {
        "CompanyName":"Eastern Connection",
        "ContactName":"Ann Devon",
        "ContactTitle":"Sales Agent",
        "Address":"35 King George",
        "City":"London",
        "Country":"UK"
      },
      {
        "CompanyName":"Ernst Handel",
        "ContactName":"Roland Mendel",
        "ContactTitle":"Sales Manager",
        "Address":"Kirchgasse 6",
        "City":"Graz",
        "Country":"Austria"
      }
    ];

    this.select_datafields_list = [      { name: 'CompanyName', type: 'string' },
                                          { name: 'ContactName', type: 'string' },
                                          { name: 'ContactTitle', type: 'string' },
                                          { name: 'Address', type: 'string' },
                                          { name: 'City', type: 'string' },
                                          { name: 'Country', type: 'string' }
                                    ];

   this.columns_list = [
     { text: 'Company Name', datafield: 'CompanyName', width: 250 },
     { text: 'Contact Name', datafield: 'ContactName', width: 150 },
     { text: 'Contact Title', datafield: 'ContactTitle', width: 180 },
     { text: 'City', datafield: 'City', width: 120 },
     { text: 'Country', datafield: 'Country'}
   ];

    if (this.selectValue === 'data') {
      this.select_data_list =
        [{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" },{ "가입날짜": "2015.01.01", "아이디": "Maria Anders", "고객명": "사나", "이메일": "sana@naver.com", "전화번호": "010-4222-2323", "카메라수": 5,
          "주소":"서울특별시강서구",
          "상태":"휴먼" }, { "가입날짜": "2014.01.01", "아이디": "arigatto", "고객명": "지효", "이메일": "jihue@naver.com", "전화번호": "010-2332-2323", "카메라수": 51,
          "주소":"서울특별시양천구",
          "상태":"사용" }, { "가입날짜": "2012.01.01", "아이디": "momomiea", "고객명": "미나", "이메일": "mina@naver.com", "전화번호": "010-4005-4523", "카메라수": 156,
          "주소":"서울특별시마포",
          "상태":"사용" }];

      this.select_datafields_list = [      { name: '가입날짜', type: 'string' },
                                            { name: '아이디', type: 'string' },
                                            { name: '고객명', type: 'string' },
                                            { name: '이메일', type: 'string' },
                                            { name: '전화번호', type: 'string' },
                                            { name: '카메라수', type: 'number' },
                                            { name: '주소', type: 'string' },
                                            { name: '상태', type: 'string' }
                                     ];

      this.columns_list = [
        { text: '가입날짜', datafield: '가입날짜', width: 100 },
        { text: '아이디', datafield: '아이디', width: 120 },
        { text: '고객명 Title', datafield: '고객명', width: 50 },
        { text: '이메일', datafield: '이메일', width: 180 },
        { text: '전화번호', datafield: '전화번호'},
        { text: '카메라수', datafield: '카메라수'},
        { text: '주소', datafield: '주소'},
        { text: '상태', datafield: '상태'}
      ];
    }

    let source =
      {
        datatype: "json",
        datafields: this. select_datafields_list,
        localdata: this.select_data_list
      };

    let dataAdapter = new $.jqx.dataAdapter(source, {
      loadComplete: function (localdata) { },
      loadError: function (xhr, status, error) { }
    });
    let self= this;



    // 페이징 UI 위치 변경 - paggerrenderer 함수 이용
     this.pagerrenderer = function () {

        let element = $("<div style='margin-left: 10px; margin-top: 5px; width: 100%; height: 100%;'></div>");
        let datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
        let paginginfo = datainfo.paginginformation;
        let leftButton = $("#left");

          leftButton.find('div').addClass('jqx-icon-arrow-left');
          leftButton.width(36);
          leftButton.jqxButton({
            theme: 'energyblue'
          });

        let rightButton = $("#right");
          rightButton.find('div').addClass('jqx-icon-arrow-right');
          rightButton.width(36);
          rightButton.jqxButton({
             theme: 'energyblue'
           });
            leftButton.appendTo('#left');
            rightButton.appendTo('#right');

          let label = $('#label');
            label.text("1-" + paginginfo.pagesize + ' of ' + datainfo.rowscount);
            label.appendTo('#label');
            self.label = label;

      // update buttons states.
      let handleStates = function (event, button, className, add) {
        button.on(event, function () {
          if (add == true) {
            button.find('div').addClass(className);
          } else button.find('div').removeClass(className);
        });
      }
      rightButton.click(function () {
        $("#jqxgrid").jqxGrid('gotonextpage');
      });
      leftButton.click(function () {
        $("#jqxgrid").jqxGrid('gotoprevpage');
      });
      return element;
    }



   $("#jqxgrid").jqxGrid(
      {
        source: dataAdapter,
        //columns:this.columns_data_list,
        columnsresize: true,
        columns: this.columns_list,
        pageable: true,                                                        // 페이징처리
        pagerrenderer: this.pagerrenderer,                                     // 페이지 랜더링
        sortable: true,                                                        // 정렬
        editable: true,                                                        // 페이지 수정
        selectionmode: 'checkbox',                                              // 체크박스 기능
        autoHeight: false,                                                      // 세로 스크롤 기능
        width: 680
        // editmode: 'selectedrow',                                             // 페이지 수정 옵션
        // filterable: true,                                                    // 검색 기능
        // showfilterrow: true,                                                 // 검색 기능

      });

    // 페이징처리 - 페이지 이동
    $("#jqxgrid").on('pagechanged', function () {
      let datainfo = $("#jqxgrid").jqxGrid('getdatainformation');
      let paginginfo = datainfo.paginginformation;
      self.label.text(1 + paginginfo.pagenum * paginginfo.pagesize + "-" + Math.min(datainfo.rowscount, (paginginfo.pagenum + 1) * paginginfo.pagesize) + ' of ' + datainfo.rowscount);
    });


    // 옵션 - paging 처리 On/Off
    $("#jqxCheckBox_paging").jqxCheckBox({ width: 150, height: 25, checked: true});
      $("#jqxCheckBox_paging").on('change', function (event) {
        let checked = event.args.checked;
        if (checked) {
          $("#jqxCheckBox_paging").find('span')[1].innerHTML = 'Paging On';
          $('#jqxgrid').jqxGrid({ pageable:true});
          $('#label').show();
          $('#button').show();
        }
        else {
          $("#jqxCheckBox_paging").find('span')[1].innerHTML = 'Paging Off';
          $('#jqxgrid').jqxGrid({pageable:false});
          $('#label').hide();
          $('#button').hide();

        }
    });

    // 옵션 - 체크박스 On/Off
    $("#jqxCheckBox_check").jqxCheckBox({ width: 140, height: 25, checked: true});
    $("#jqxCheckBox_check").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_check").find('span')[1].innerHTML = 'Checkbox On';
        $('#jqxgrid').jqxGrid({selectionmode: 'checkbox'});
      }
      else {
        $("#jqxCheckBox_check").find('span')[1].innerHTML = 'Checkbox Off';
        $('#jqxgrid').jqxGrid({selectionmode: 'none'});
      }

    });

    // 옵션 - editing On/Off
    $("#jqxCheckBox_edit").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_edit").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        //this.paging_option = true;
        $("#jqxCheckBox_edit").find('span')[1].innerHTML = 'Editing On';
        $('#jqxgrid').jqxGrid({ editable:true});
      }
      else {
        $("#jqxCheckBox_edit").find('span')[1].innerHTML = 'Editing Off';
        $('#jqxgrid').jqxGrid({editable:false});
      }
    });

    // 옵션 - Scroll On/Off
    $("#jqxCheckBox_scroll").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_scroll").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_scroll").find('span')[1].innerHTML = 'Scroll On';
        $('#jqxgrid').jqxGrid({ autoHeight: false});
      }
      else {
        $("#jqxCheckBox_scroll").find('span')[1].innerHTML = 'Scroll Off';
        $('#jqxgrid').jqxGrid({autoHeight: true});
      }
    });

    // 옵션 - sorting On/Off
    $("#jqxCheckBox_sort").jqxCheckBox({ width: 150, height: 25, checked: true});
    $("#jqxCheckBox_sort").on('change', function (event) {
      let checked = event.args.checked;
      if (checked) {
        $("#jqxCheckBox_sort").find('span')[1].innerHTML = 'Sorting On';
        $('#jqxgrid').jqxGrid({  sortable: true,  showsortmenuitems: true });
      }
      else {
        $("#jqxCheckBox_sort").find('span')[1].innerHTML = 'Sorting Off';
        $('#jqxgrid').jqxGrid({ sortable: false,  showsortmenuitems: false });
      }
    });


    // // 대용량 데이터 페이징 처리
    // $('#jqxbutton').click(function () {
    //   let datainformation = $('#jqxGrid').jqxGrid('getdatainformation');
    //   let rowscount = datainformation.rowscount;
    //
    //   let paginginformation = $('#jqxgrid').jqxGrid('getpaginginformation');
    //   // The page's number.
    //   let pagenum = paginginformation.pagenum;
    //   // The page's size.
    //   let pagesize = paginginformation.pagesize;
    //   // The number of all pages.
    //   let pagescount = paginginformation.pagescount;
    // });


    // create new row.
    $("#addrow").bind('click', function () {
      let commit = $("#jqxgrid").jqxGrid('addrow',null, {}, "first");
    });

    //delete row multi
    $("#deleterow").click(function () {
      let rowIndexes = $('#jqxgrid').jqxGrid('getselectedrowindexes');
      let rowIds = new Array();
      for (let i = 0; i < rowIndexes.length; i++) {
        let currentId = $('#jqxgrid').jqxGrid('getrowid', rowIndexes[i]);
        rowIds.push(currentId);
      };
      $('#jqxgrid').jqxGrid('deleterow', rowIds);
      $('#jqxgrid').jqxGrid('clearselection');
    });

    // export
    $("#export").click(function () {
      $("#jqxgrid").jqxGrid('exportdata', 'xls', 'jqxGrid', false);
    });

    // Print
    $("#print").click(function () {
      var gridContent = $("#jqxgrid").jqxGrid('exportdata', 'html');
      var newWindow = window.open('', '', 'width=800, height=500'),
        document = newWindow.document.open(),
        pageContent =
          '<!DOCTYPE html>\n' +
          '<html>\n' +
          '<head>\n' +
          '<meta charset="utf-8" />\n' +
          '<title>jQWidgets Grid</title>\n' +
          '</head>\n' +
          '<body>\n' + gridContent + '\n</body>\n</html>';
      document.write(pageContent);
      document.close();
      newWindow.print();
    });

    // search
    // clear filters.
    $("#clearButton").click(function () {
      $("#jqxgrid").jqxGrid('clearfilters');
    });
    // find records that match a criteria.
    $("#findButton").click(function () {
      $("#jqxgrid").jqxGrid('clearfilters');
      //let searchColumnIndex = $("#dropdownlist").jqxDropDownList('selectedIndex');
      let datafield = "";
      // switch (searchColumnIndex) {
      //   case 0:
      //     datafield = "firstname";
      //     break;
      //   case 1:
      //     datafield = "lastname";
      //     break;
      //   case 2:
      //     datafield = "productname";
      //     break;
      //   case 3:
      //     datafield = "quantity";
      //     break;
      //   case 4:
      //     datafield = "price";
      //     break;
      // }
      let searchText = $("#inputField").val();
      let filtergroup = new $.jqx.filter();
      let filter_or_operator = 1;
      let filtervalue = searchText;
      let filtercondition = 'contains';
      let filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
      filtergroup.addfilter(filter_or_operator, filter);
      $("#jqxgrid").jqxGrid('addfilter', "firstname", filtergroup);
      // apply the filters.
      $("#jqxgrid").jqxGrid('applyfilters');
    });

  }


// 그리드데이터 API 호출 ( xhr 이용한 호출,  observable 수정 작업 필요 )
  getApiData(){

    let http = new XMLHttpRequest();
    let url = ``;
    let contenttype = '';
    let token = '';
    const params = '';
    let results:JSON

    http.open('GET', url, true);  // api 호출
    http.setRequestHeader('', ''); // 요청시 header값 추가
    http.setRequestHeader('', ''); // 요청시 header값 추가

    // api 호출 성공 시
    if(http.status == 200){
      // 성공 시 data input 예시
      this.fristname_data_list   = JSON.parse(JSON.stringify(results));  // 이름 데이터 리스트에 결과값 저장
      this.lastname_data_list    = JSON.parse(JSON.stringify(results));  // 이름(성) 데이터 리스트에 결과값 저장
      this.productname_data_list = JSON.parse(JSON.stringify(results));  // 물품명 데이터 리스트에 결과값 저장
      this.pricevalue_data_list  = JSON.parse(JSON.stringify(results));  // 가격 데이터 리스트에 결과값 저장
      this.columns_data_list     = JSON.parse(JSON.stringify(results));  // 컬럼데이터 리스트에 결과값 저장

      let data_Info
      // data 배열 array 입력 예시
      $.each(data_Info, (idx, data) => {
        // push .ex
        this.columns_data_list.push(data);
      })
    }
  }



}

