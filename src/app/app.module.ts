import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutComponent } from './layout/layout.component';
import { JstreeComponent } from './jstree/jstree.component';
import { JqwidgetGridComponent } from './jqwidgets/jqwidget-grid/jqwidget-grid.component';
import { JqwidgetChartsComponent } from './jqwidgets/jqwidget-charts/jqwidget-charts.component';
import { JqwidgetTreeComponent } from './jqwidgets/jqwidget-tree/jqwidget-tree.component';
import { JuiChartsComponent } from './jui/jui-charts/jui-charts.component';
import { JuiGridComponent } from './jui/jui-grid/jui-grid.component';
import { JqwidgetWithComponent } from './jqwidgets/jqwidget-with/jqwidget-with.component';
import { RealtimeComponent } from './timeline/realtime/realtime.component';
import { PlaybackComponent } from './timeline/playback/playback.component';
import { GisComponent } from './gis/gis.component';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { SvgComponent } from './e-map/svg/svg.component';
import { ImageComponent } from './e-map/image/image.component';
import { SafeHtmlPipe } from './e-map/image/safe-html.pipe';
import { LeafletComponent } from './e-map/leaflet/leaflet.component';

@NgModule({
  declarations: [
    AppComponent,
    LayoutComponent,
    JstreeComponent,
    JqwidgetGridComponent,
    JqwidgetChartsComponent,
    JqwidgetTreeComponent,
    JuiChartsComponent,
    JuiGridComponent,
    JqwidgetWithComponent,
    RealtimeComponent,
    PlaybackComponent,
    GisComponent,
    SvgComponent,
    ImageComponent,
    SafeHtmlPipe,
    LeafletComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

