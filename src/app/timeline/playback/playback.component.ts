import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-playback',
  templateUrl: './playback.component.html',
  styleUrls: ['./playback.component.css']
})
export class PlaybackComponent implements OnInit {

  constructor() { }

  ngOnInit() {

    this.getTime();
  }

  getTime() {
    const vid = $('#myvideo')[0];

    // console.log(vid.currentTime);
    // console.log(vid.duration);
    // const stop = document.getElementById('myvideo').pause();

    const data = new vis.DataSet([
      {
        id : 1,
        start : '2014-04-20'
      }
    ]);

    const options = {};

    const container = $('#mytimeline')[0];
    const timeline = new vis.Timeline(container, data, options);

    // timeline.addCustomTime(new Date());

     const start = new Date((new Date().getTime()) - 2 * 60 * 1000);
     const end = new Date((new Date().getTime()) + 3 * 60 * 1000);

     const ay = new Date((vid.currentTime) - 2 * 60 * 1000);

     // console.log(ay);
    // timeline.setWindow(start, end, {animation: false});

  }

}
