import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-realtime',
  templateUrl: './realtime.component.html',
  styleUrls: ['./realtime.component.css']
})
export class RealtimeComponent implements OnInit {

  constructor() {}
  container: any;

  ngOnInit() {
    this.customTimeline();
  }

  customTimeline() {

    const data = new vis.DataSet([
      {
        id: 1,
        start: new Date((new Date()).getTime() - 60 * 1000),
        content : '헷'
      }
    ]);
    const options = {
      showCurrentTime: true,
    };
      // create a timeline
    this.container = $('#customTime')[0];
    const timeline = new vis.Timeline(this.container, data, options);

    timeline.addCustomTime(new Date().valueOf());

    // timeline.on('currentTimeTick', function () {
    //   timeline.setCustomTime(new Date().valueOf());
    // });

    $('#customTime').click(() => {
      const props = timeline.getEventProperties(event);
        timeline.setCustomTime(props['time'].valueOf());
        timeline.moveTo(props['time'].valueOf());
    });

    function move (percentage) {

      const range = timeline.getWindow();
      const interval = range.end - range.start;

        timeline.setWindow({
          start: range.start.valueOf() - interval * percentage,
          end: range.end.valueOf() - interval * percentage
        });

    }
      // current time's range 정의
    const start = new Date((new Date()).getTime() - 2 * 60 * 1000);
    const end = new Date((new Date()).getTime() + 3 * 60 * 1000);
    timeline.setWindow(start, end, {animation: false});

    $('#zoomIn').click( () => { timeline.zoomIn( 0.2 ); });
    $('#zoomOut').click( () => { timeline.zoomOut( 0.2 ); });
    $('#moveLeft').click( () => { move( 0.2); });
    $('#moveRight').click( () => { move(-0.2); });
    $('#goalive').click( () => {
      const currentTime = timeline.getCurrentTime();
      timeline.moveTo(currentTime);
      timeline.setCustomTime(currentTime);
    });
    $('#10m').click( () => {
      const start = new Date((new Date()).getTime() - 5 * 60 * 1000);
      const end = new Date((new Date()).getTime() + 5 * 60 * 1000);
      timeline.setWindow(start, end, {animation: true});
    });
    $('#60m').click( () => {
      const start = new Date((new Date()).getTime() - 30 * 60 * 1000);
      const end = new Date((new Date()).getTime() + 30 * 60 * 1000);
      timeline.setWindow(start, end, {animation: true});
    });
    $('#6h').click( () => {
      const start = new Date((new Date()).getTime() - 180 * 60 * 1000);
      const end = new Date((new Date()).getTime() + 180 * 60 * 1000);
      timeline.setWindow(start, end, {animation: true});
    });
    $('#24h').click( () => {
      const start = new Date((new Date()).getTime() - 720 * 60 * 1000);
      const end = new Date((new Date()).getTime() + 720 * 60 * 1000);
      timeline.setWindow(start, end, {animation: true});
    });
  }
}
