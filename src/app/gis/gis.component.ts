import { Component, OnInit } from '@angular/core';
import { rootRenderNodes } from '@angular/core/src/view/util';
import { last } from '@angular/router/src/utils/collection';

declare const google: any;
declare const $: any;
declare const MarkerClusterer: any;
@Component({
  selector: 'app-gis',
  templateUrl: './gis.component.html',
  styleUrls: ['./gis.component.css']
})
export class GisComponent implements OnInit {

  markers: any;
  overlay: any;
  pos: any;

  constructor() { }

  ngOnInit() {
    this.createMap();
  }

  createMap() {

    const myLatLng = new google.maps.LatLng(37.6646797, 126.74212220000004);
    const mapOptions = {
      zoom: 1,
      center: myLatLng,
      mapTypeId: google.maps.MapTypeId.READMAP,
      // draggable: false
    };

    const map = new google.maps.Map($('#map')[0], mapOptions);

    // this.setMarkers(map);
    this.setMarkers(map,-33.950190, 140.259302);
    //const map2 = new google.maps.Map($('#map')[0], mapOptions);
    this.setMarkers(map,37.6646797, 126.74212220000004);

    const infowindow = new google.maps.InfoWindow;
    const geocoder = new google.maps.Geocoder;
    // this.setMarkers(map);

    $('#submit1').click( () => {
      const geo = $('#submit1').val();
      this.geocoding(geocoder, map, infowindow, geo);
    });

    $('#submit2').click( () => {
      const geo = $('#submit2').val();
      this.geocoding(geocoder, map, infowindow, geo);
    });

    const drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode : google.maps.drawing.OverlayType.MARKER,
      drawingControl: true,
      map: map,
      drawingControlOptions: {
        position: google.maps.ControlPosition.TOP_CENTER,
        drawingModes: ['polygon']
      },
      markerOptions: {icon: '../../assets/img/cctv_45_sky.png'},
      polygonOptions: {
        clickable : true,
        strokeColor: '#FF0000',
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: '#FF0000',
        fillOpacity: 0.35
        // editable: true,
        // draggable : true
      }
    });
    // const path = this.getPath();
    drawingManager.setMap(map);

    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
      const polygon1 = polygon.getPath();

      console.log(polygon1);

      for (let i = 0; i < polygon1.getLength(); i++) {
        const xy = polygon1.getAt(i);
        // console.log(xy.lat() + '/' + xy.lng());
        const contentString = xy.lat() + '/' + xy.lng();
      }
    });

  }

  geocoding(geocoder, map, infowindow , geo) {
    const image = {
      url: '../../assets/img/cctv_45_sky.png'
    };
    if (geo === 'Geocode') {

      const address = $('#address').val();
      geocoder.geocode({ 'address': address}, (results, status) => {
        if (status === 'OK') {
          map.setCenter(results[0].geometry.location);
          const marker = new google.maps.Marker({
            map: map,
            position: results[0].geometry.location,
            // icon : image,
          });
          infowindow.setContent('좌표 : ' + results[0].geometry.location);
          infowindow.open(map, marker);
        }else {
          alert (status);
        }
      });

    }else {

      const input = $('#latlng').val();
      const latlngStr = input.split(',', 2);
      const latlng = {lat: parseFloat(latlngStr[0]), lng: parseFloat(latlngStr[1])};
      geocoder.geocode({'location': latlng}, (results, status) => {
        if (status === 'OK') {
          if (results[1]) {
            map.setZoom(11);
            const marker = new google.maps.Marker({
              position: latlng,
              map: map,
              // icon : image
            });
            infowindow.setContent(results[1].formatted_address);
            infowindow.open(map, marker);
          } else {
            window.alert('No results found');
          }
        } else {
          window.alert('Geocoder failed due to: ' + status);
        }
      });
    }
  }

  setMarkers(map,_lat,_lng) {
    const locations = [
      ['VMS01', 37.6646797, 126.74212220000004, 10],
      ['VMS02', -33.950190, 140.259302, 9],
      ['VMS03', -33.890542, 151.274856, 8],
      ['VMS04', -33.923036, 151.259052, 7],
      ['VMS05', -34.028249, 151.157507, 6],
      ['VMS06', -33.80010128657071, 151.28747820854187, 5],
      ['VMS07', -32.950198, 151.259302, 4],
      ['VMS08', -31.950198, 151.259302, 3],
      ['VMS09', -32.950108, 151.259302, 2],
      ['VMS10', -32.950190, 140.259302, 1]
    ];

    function HTMLMarker(lat, lng) {
      console.log(lat);
      this.lat = lat;
      this.lng = lng;
      this.pos = new google.maps.LatLng(lat, lng);
    }

    for ( let i = 0; i < locations.length; i++) {

      // HTMLMarker.prototype = new google.maps.OverlayView();

      //   HTMLMarker.prototype.onRemove = function() {};

      //   HTMLMarker.prototype.onAdd = function(){

      //     const img = document.createElement('img');
      //     img.id = 'test-img' + i;
      //     img.src = '../../video/cameraIcon.png';
      //     const panes = this.getPanes();
      //     panes.overlayImage.appendChild(img);

      //     // console.log($('#test-img'));
      //     const pw = $('#test-img');

      //     $('#rotate').on('click', () => {

      //       $('#test-img' + i).on('mousemove', (e) => {

      //         console.log(i);

      //         const mk = $('#test-img' + i);
      //         const offset = mk.offset();
      //         console.log(offset);
      //         const center_x = (offset.left) + ($(mk).width() / 2);
      //         const center_y = (offset.top) + ($(mk).height() / 2);
      //         const mouse_x = e.pageX;
      //         const mouse_y = e.pageY;
      //         const radians = Math.atan2(mouse_x - center_x, mouse_y - center_y);
      //         const degree = (radians * (180 / Math.PI) * -1) + 100;
      //         $(mk).css('-webkit-transform', 'rotate(' + degree + 'deg)');
      //         $(mk).css('-ms-transform', 'rotate(' + degree + 'deg)');
      //         const listener1 = map.addListener('click', Function);
      //         google.maps.event.removeListener(listener1);

      //       });

      //       $('#test-img' + i).on('mouseup', () => {
      //         $('#test-img' + i).off('mousemove');
      //       });

      //     });

      //   };

      //   HTMLMarker.prototype.draw = function() {

      //     const overlayProjection = this.getProjection();
      //     const position = overlayProjection.fromLatLngToDivPixel(this.pos);
      //     const panes = this.getPanes();
      //     panes.overlayImage.style.left = position.x + 'px';
      //     panes.overlayImage.style.top = position.y - 30 + 'px';

      //   };

      //   // const htmlMarker = [];
      //   /// htmlMarker.push = new HTMLMarker(locations[i][1], locations[i][2]);
      //   // htmlMarker[i].setMap(map);
      //   const htmlMarker = new HTMLMarker(37.6646797, 126.74212220000004);
      //   htmlMarker.setMap(map);

    }

    HTMLMarker.prototype = new google.maps.OverlayView();

    HTMLMarker.prototype.onRemove = function() {};

    HTMLMarker.prototype.onAdd = function(){
      var div = document.createElement('DIV');
      div.style.position='absolute';
      div.className = "htmlMarker";
      div.innerHTML = "<img src='../../assets/img/cctv_45_sky.png'>";

      const panes = this.getPanes();
      //panes.overlayImage.appendChild(img);
      panes.overlayImage.appendChild(div);
      this.div=div;

    };

    HTMLMarker.prototype.draw = function() {
      const overlayProjection = this.getProjection();
      const position = overlayProjection.fromLatLngToDivPixel(this.pos);
      const panes = this.getPanes();

      // panes.overlayImage.style.left = position.x + 'px';
      this.div.style.left = position.x + 'px';
      this.div.style.top = position.y - 30 + 'px';
      // panes.overlayImage.style.top = position.y - 30 + 'px';


    };

    // const htmlMarker = [];
    /// htmlMarker.push = new HTMLMarker(locations[i][1], locations[i][2]);
    // htmlMarker[i].setMap(map);
    const htmlMarker = new HTMLMarker(_lat, _lng);
    htmlMarker.setMap(map);
    // const rotation = 10;

    // const Symbol    = {
    //   // url: '../../video/cameraIcon.png',
    //   rotation: rotation,
    //   path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
    //   // path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
    //   scale: 5
    // };

    // const markers = [];
    // for (let i = 0; i < locations.length; i++ ) {

    //     const location = locations[i];

    //     const marker = new google.maps.Marker({
    //       position : {lat: location[1], lng: location[2]},
    //       map : map,
    //       // icon: Symbol,
    //       title : location[0],
    //       clickable: true,
    //       id: i,
    //       icon : {
    //         url : '../../video/cameraIcon.png'
    //       }
    //     });

    //     const infowindow = new google.maps.InfoWindow({
    //       // tslint:disable-next-line:max-line-length
    //         content : '<div style= "width: 100px">' +  location[0] + '</div>' + '<video id="myvideo" autoplay preload="auto" width="290px" height="160px"><source src="../../video/Live1.mp4" type="video/mp4"></video>'
    //     });

    //     marker.addListener('click', (event) => {
    //       infowindow.open(map, marker);
    //     });

    //     marker.addListener('mousedown', (event) => {
    //      // this. rotateOnMouse(markers[i]);
    //     });

    //     marker.addListener('mouseup', () => {
    //     });

    //     markers.push(marker);
    // }
    //   const markerCluster = new MarkerClusterer(map, markers,
    //       {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'}
    //   );
  }

}
