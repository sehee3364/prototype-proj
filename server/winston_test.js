const winston = require('winston');

var logger = new (winston.Logger)({
  transports: [
    new (winston.transports.Console)({
      level: 'debug',
      silent: false,
      colorize: false,
      timestamp: false
    }),
    new (winston.transports.File)({
      level: 'debug',
      silent: false,
      colorize: false,
      timestamp: false,
      filename: 'app-logging.log',
      maxsize: 1024,
      maxFiles: 3,
      json: true
    })
  ]
});

module.exports = logger;
